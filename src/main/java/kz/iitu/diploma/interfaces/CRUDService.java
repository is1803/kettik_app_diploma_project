package kz.iitu.diploma.interfaces;

import java.util.List;

public interface CRUDService<T> {
    T create(T t);

    List<T> read();
    T read(int id);

    T update(int id, T t);

    void delete(int id);
}
