//package com.example.test_user_login_email_verification_create_authentication.tours.exceptions;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//@ResponseStatus(code = HttpStatus.NOT_FOUND)
//public class CityNotFoundException extends RuntimeException{
//    public CityNotFoundException() {
//        super("City not found");
//    }
//
//    public CityNotFoundException(String message) {
//        super(message);
//    }
//
//    public CityNotFoundException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public CityNotFoundException(Throwable cause) {
//        super(cause);
//    }
//}
