package kz.iitu.diploma.tours.exceptions;

public class NotFoundException extends RuntimeException {

    private String resource;
    private int id;
    private String email;

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String resource, int id) {
        super();
        this.resource = resource;
        this.id = id;
    }
}
