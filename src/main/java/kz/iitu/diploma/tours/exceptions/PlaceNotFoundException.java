//package com.example.test_user_login_email_verification_create_authentication.tours.exceptions;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.ResponseStatus;
//
//@ResponseStatus(code = HttpStatus.NOT_FOUND)
//public class PlaceNotFoundException extends RuntimeException{
//    public PlaceNotFoundException() {
//        super("Place not found");
//    }
//
//    public PlaceNotFoundException(String message) {
//        super(message);
//    }
//
//    public PlaceNotFoundException(String message, Throwable cause) {
//        super(message, cause);
//    }
//
//    public PlaceNotFoundException(Throwable cause) {
//        super(cause);
//    }
//}
