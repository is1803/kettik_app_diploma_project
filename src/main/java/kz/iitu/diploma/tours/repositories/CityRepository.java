package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City,Integer>, JpaSpecificationExecutor<City> {
}
