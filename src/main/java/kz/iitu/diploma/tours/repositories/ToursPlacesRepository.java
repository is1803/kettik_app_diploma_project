package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.ToursItems;
import kz.iitu.diploma.tours.models.ToursPlaces;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToursPlacesRepository extends JpaRepository<ToursPlaces, Integer>, JpaSpecificationExecutor<ToursPlaces> {

    List<ToursPlaces> findByTourId(int id);
    ToursPlaces findByPlaceId(int id);

}
