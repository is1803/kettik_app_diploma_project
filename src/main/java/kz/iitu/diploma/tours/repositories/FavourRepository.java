package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.Favour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface FavourRepository extends JpaRepository<Favour, Integer>, JpaSpecificationExecutor<Favour> {
}
