package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.ToursItems;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ToursItemsRepository extends JpaRepository<ToursItems, Integer>, JpaSpecificationExecutor<ToursItems> {

    List<ToursItems> findByTourId(int id);
    List<ToursItems> findByItemId(int id);

}
