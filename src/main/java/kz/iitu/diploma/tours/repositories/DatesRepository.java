package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.Dates;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DatesRepository extends JpaRepository<Dates, Integer>, JpaSpecificationExecutor<Dates> {
}
