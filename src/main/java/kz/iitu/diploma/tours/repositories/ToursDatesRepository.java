//package kz.iitu.diploma.tours.repositories;
//
//import kz.iitu.diploma.tours.models.ToursDates;
//import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface ToursDatesRepository extends JpaRepository<ToursDates, Integer>, JpaSpecificationExecutor<ToursDates> {
//    List<ToursDates> findByTourId(int id);
//    List<ToursDates> findByDateId(int id);
//}