package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.Tour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TourRepository extends JpaRepository<Tour, Integer>, JpaSpecificationExecutor<Tour> {

    List<Tour> findByCityId(int id);
    List<Tour> findByTitle(String title);
    List<Tour> findByOwnerId(int id);
}
