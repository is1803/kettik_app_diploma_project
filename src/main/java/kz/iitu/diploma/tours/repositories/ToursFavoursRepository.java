package kz.iitu.diploma.tours.repositories;

import kz.iitu.diploma.tours.models.ToursFavours;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ToursFavoursRepository extends JpaRepository<ToursFavours, Integer>, JpaSpecificationExecutor<ToursFavours> {
    List<ToursFavours> findByTourId(int id);
    List<ToursFavours> findByFavourId(int id);
}
