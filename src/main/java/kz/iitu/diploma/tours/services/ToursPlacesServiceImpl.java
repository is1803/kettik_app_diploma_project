package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.models.ToursItems;
import kz.iitu.diploma.tours.models.ToursPlaces;
import kz.iitu.diploma.tours.repositories.ToursPlacesRepository;
import kz.iitu.diploma.tours.services.interfaces.ToursPlacesService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToursPlacesServiceImpl implements ToursPlacesService {

    private final ToursPlacesRepository repository;

    public ToursPlacesServiceImpl(ToursPlacesRepository repository) {
        this.repository = repository;
    }

    @Override
    public ToursPlaces create(ToursPlaces toursPlaces) {
        return repository.save(ToursPlaces.builder()
                        .placeId(toursPlaces.getPlaceId())
                        .tourId(toursPlaces.getTourId())
                        .build());
    }

    @Override
    public List<ToursPlaces> read() {
        return repository.findAll();
    }

    @Override
    public ToursPlaces read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public ToursPlaces update(int id, ToursPlaces toursPlaces) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    public List<ToursPlaces> findAllByTourId(int id) {
        return repository.findByTourId(id);
    }

}
