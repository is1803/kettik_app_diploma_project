package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.tour.DatesDTO;
import kz.iitu.diploma.tours.models.Dates;
import kz.iitu.diploma.tours.repositories.DatesRepository;
import kz.iitu.diploma.tours.services.interfaces.DatesService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class DatesServiceImpl implements DatesService {

    private final DatesRepository repository;

    public DatesServiceImpl(DatesRepository repository) {
        this.repository = repository;
    }

    @Override
    public Dates create(Dates tourDates) {
        return repository.save(Dates.builder()
                                .fromDate(tourDates.getFromDate())
                                .toDate(tourDates.getToDate())
                                .maxCountOfPerson(tourDates.getMaxCountOfPerson())
                                .build());
    }

    public Dates create(DatesDTO tourDates) {
        return repository.save(Dates.builder()
                .fromDate(tourDates.getFromDate())
                .toDate(tourDates.getToDate())
                .maxCountOfPerson(tourDates.getMaxCountOfPerson())
                .build());
    }

    @Override
    public List<Dates> read() {
        return repository.findAll();
    }

    @Override
    public Dates read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public Dates update(int id, Dates tourDates) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

}
