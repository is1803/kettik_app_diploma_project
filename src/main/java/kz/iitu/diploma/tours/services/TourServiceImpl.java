package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.tour.FinalTourDTO;
import kz.iitu.diploma.tours.DTO.tour.TourAddDTO;
import kz.iitu.diploma.tours.DTO.tour.DatesDTO;
import kz.iitu.diploma.tours.models.*;
import kz.iitu.diploma.tours.repositories.TourRepository;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.services.UserServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TourServiceImpl  {

    private final ToursItemsServiceImpl toursItemsService;
    private final ToursFavoursServiceImpl favoursService;
    private final ToursPlacesServiceImpl placesService;
    private final DatesServiceImpl tourDatesService;
    private final TourRepository repository;
    private final CityServiceImpl cityService;
    private final UserServiceImpl userService;
    private final ItemServiceImpl itemService;
    private final PlaceServiceImpl placeService;
    private final FavourServiceImpl favourService;

    public TourServiceImpl(DatesServiceImpl tourDatesService, ToursItemsServiceImpl toursItemsService,
                           ToursFavoursServiceImpl favoursService, ToursPlacesServiceImpl placesService,
                           TourRepository repository, CityServiceImpl cityService,
                           UserServiceImpl userService, ItemServiceImpl itemService, PlaceServiceImpl placeService,
                           FavourServiceImpl favourService) {
        this.tourDatesService = tourDatesService;
        this.toursItemsService = toursItemsService;
        this.favoursService = favoursService;
        this.placesService = placesService;
        this.repository = repository;
        this.cityService = cityService;
        this.userService = userService;
        this.itemService = itemService;
        this.placeService = placeService;
        this.favourService = favourService;
    }

    public List<FinalTourDTO> all() {
        List<Tour> tours = repository.findAll();
        List<FinalTourDTO> finalTourDTOList = new ArrayList<>();
        List<Integer> toursId = new ArrayList<>();

        for (int i = 0; i < tours.size(); i++) {
            toursId.add(tours.get(i).getId());
        }

        for (int i = 0; i < toursId.size(); i++) {
            FinalTourDTO finalTourDTO = findOne(toursId.get(i));
            finalTourDTOList.add(finalTourDTO);
        }

        return finalTourDTOList;
    }

    public List<FinalTourDTO> getByCity(int cityId){

        List<Integer> toursId = new ArrayList<>();
        List<FinalTourDTO> finalTourDTOList = new ArrayList<>();
        List<Tour> tours = repository.findByCityId(cityId);

        for (int i = 0; i < tours.size(); i++) {
            // list tours with almaty city
            toursId.add(tours.get(i).getId());
            // 47483647, 47483648, 47483649, 47483650, 47483651, 47483652, 47483653
        }

        for (int i = 0; i < toursId.size(); i++) {
            FinalTourDTO finalTourDTO = findOne(toursId.get(i));
            finalTourDTOList.add(finalTourDTO);
        }

        return finalTourDTOList;
    }

    public List<FinalTourDTO> getByBusinessUserId(int businessUserId){

        List<Integer> toursId = new ArrayList<>();
        List<FinalTourDTO> finalTourDTOList = new ArrayList<>();
        List<Tour> tours = repository.findByOwnerId(businessUserId);

        for (int i = 0; i < tours.size(); i++) {
            // list tours with almaty city
            toursId.add(tours.get(i).getId());
            // 47483647, 47483648, 47483649, 47483650, 47483651, 47483652, 47483653
        }

        for (int i = 0; i < toursId.size(); i++) {
            FinalTourDTO finalTourDTO = findOne(toursId.get(i));
            finalTourDTOList.add(finalTourDTO);
        }

        return finalTourDTOList;
    }

    public FinalTourDTO findOne(Integer idTour) {

        Tour tour = repository.getById(idTour);
        City city = cityService.read(tour.getCityId());
        User user = userService.read(tour.getOwnerId());

        List<ToursItems> toursItemsList = toursItemsService.findAllByTourId(tour.getId());
        List<ToursPlaces> toursPlacesList = placesService.findAllByTourId(tour.getId());
        List<ToursFavours> toursFavoursList = favoursService.findAllByTourId(tour.getId());

        List<Item> items1 = new ArrayList<>();
        List<Place> places1 = new ArrayList<>();
        List<Favour> favours1 = new ArrayList<>();

        for (int i = 0; i < toursItemsList.size(); i++) { // tourId: 47483649 - ToursItems: 47483647, ToursItems: 47483648
            // 0 |             : 47483647
            ToursItems toursItems = toursItemsService.read(toursItemsList.get(i).getId());
            Item item = itemService.read(toursItems.getItemId());
            items1.add(item);
        }

        for (int i = 0; i < toursFavoursList.size(); i++) {
            ToursFavours toursFavours = favoursService.read(toursFavoursList.get(i).getId());
            Favour favour = favourService.read(toursFavours.getFavourId());
            favours1.add(favour);
        }

        for (int i = 0; i < toursPlacesList.size(); i++) {
            ToursPlaces toursPlaces = placesService.read(toursPlacesList.get(i).getId());
            Place place = placeService.read(toursPlaces.getPlaceId());
            places1.add(place);
        }

        return FinalTourDTO.builder()
                .tourId(tour.getId())
                .title(tour.getTitle())
                .description(tour.getDescription())
                .price(tour.getPrice())
                .city(city)
                .user(user)
                .items(items1)
                .favours(favours1)
                .places(places1)
                .tourDates(tourDatesService.read(tour.getTourDateId()))
                .build();
    }

    public void createTour(TourAddDTO tourAddDTO){

        List<DatesDTO> tourDatesList = tourAddDTO.getTourDatesList();
        List<Item> itemsList = tourAddDTO.getItems();
        List<Favour> favourList = tourAddDTO.getFavours();
        List<Place> placeList = tourAddDTO.getPlaces();

        for (int i = 0; i < tourDatesList.size(); i++) {
            Dates tourDates = tourDatesService.create(tourDatesList.get(i));

            Tour tour = Tour.builder()
                    .title(tourAddDTO.getTitle())
                    .description(tourAddDTO.getDescription())
                    .price(tourAddDTO.getPrice())
                    .ownerId(tourAddDTO.getOwnerId())
                    .cityId(tourAddDTO.getCityId())
                    .tourDateId(tourDates.getId())
                    .build();
            repository.save(tour);

            for (int j = 0; j < itemsList.size(); j++) {
                ToursItems toursItems = ToursItems.builder()
                        .itemId(itemsList.get(j).getId())
                        .tourId(tour.getId())
                        .build();
                toursItemsService.create(toursItems);
            }

            for (int j = 0; j < favourList.size(); j++) {
                ToursFavours toursFavours = ToursFavours.builder()
                        .favourId(favourList.get(j).getId())
                        .tourId(tour.getId())
                        .build();
                favoursService.create(toursFavours);
            }

            for (int j = 0; j < placeList.size(); j++) {
                ToursPlaces toursPlaces = ToursPlaces.builder()
                        .placeId(placeList.get(j).getId())
                        .tourId(tour.getId())
                        .build();
                placesService.create(toursPlaces);
            }
        }
    }

//  ---------------------------------------------------------------------------------------------------------
    public List<FinalTourDTO> getByNameFourClient(String titleOfTour){

        List<Tour> tours = repository.findByTitle(titleOfTour);

        List<FinalTourDTO> finalTourDTOList = new ArrayList<>();
        List<Integer> toursId = new ArrayList<>();

        for (int i = 0; i < tours.size(); i++) {
            // list tours with almaty city
            toursId.add(tours.get(i).getId());
            // ad to list toursId only ids of tours
        }

        for (int i = 0; i < toursId.size(); i++) {
            FinalTourDTO finalTourDTO = findOne(toursId.get(i));
            finalTourDTOList.add(finalTourDTO);
        }

        return finalTourDTOList;
    }

    public List<FinalTourDTO> getByNameFourBusiness(int userId, String titleOfTour){

        List<FinalTourDTO> finalTourDTOList = new ArrayList<>();

        return finalTourDTOList;
    }
//  ---------------------------------------------------------------------------------------------------------

    public void delete(int id) {
        // tour
        Tour tour = repository.getById(id);

        // toursItems deleteByTourId
        // toursFavours deleteByTourId
        // toursPlaces deleteByTourId

        List<ToursItems> toursItemsList = toursItemsService.findAllByTourId(tour.getId());
        List<ToursPlaces> toursPlacesList = placesService.findAllByTourId(tour.getId());
        List<ToursFavours> toursFavoursList = favoursService.findAllByTourId(tour.getId());

        for (ToursItems toursItems : toursItemsList) {
            toursItemsService.delete(toursItems.getId());
        }

        for (ToursPlaces toursPlaces : toursPlacesList) {
            placesService.delete(toursPlaces.getId());
        }

        for (ToursFavours toursFavours : toursFavoursList) {
            favoursService.delete(toursFavours.getId());
        }

        // найти дату = тур.гетДатаАйди
        Dates dates = tourDatesService.read(tour.getTourDateId());
        // удалить тур
        repository.deleteById(id);
        // удалить дату
        tourDatesService.delete(dates.getId());
    }

    public Tour getOne(int id){
        return repository.findById(id).get();
    }
}
