package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.CityDTO;
import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.repositories.CityRepository;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {
   
    private final CityRepository repository;

    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public City create(City city) {
        return repository.save(city.builder()
                .name(city.getName())
                .build());
    }

    public City create(CityDTO dto) {
        return repository.save(City.builder()
                .name(dto.getName())
                .build());
    }

    @Override
    public List<City> read() {
        return repository.findAll();
    }

    @Override
    public City read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public City update(int id, City City) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    private CityDTO getUserOrThrowException(Optional<CityDTO> CityDTO) {
        if (CityDTO.isPresent()) {
            return CityDTO.get();
        }

        throw new UsernameNotFoundException("City not found in the database!");
    }
}
