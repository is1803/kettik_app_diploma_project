package kz.iitu.diploma.tours.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tours.models.ToursPlaces;

public interface ToursPlacesService extends CRUDService<ToursPlaces> {
}
