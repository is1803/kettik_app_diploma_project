package kz.iitu.diploma.tours.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tours.models.Tour;
import kz.iitu.diploma.tours.models.ToursItems;

import java.util.List;

public interface ToursItemsService extends CRUDService<ToursItems> {

}
