package kz.iitu.diploma.tours.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tours.models.Dates;

public interface DatesService extends CRUDService<Dates> {
}
