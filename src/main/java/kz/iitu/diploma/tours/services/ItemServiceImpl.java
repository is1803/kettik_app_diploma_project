package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.ItemDTO;
import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.repositories.ItemRepository;
import kz.iitu.diploma.tours.services.interfaces.ItemService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ItemServiceImpl implements ItemService {

    private final ItemRepository repository;

    public ItemServiceImpl(ItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public Item create(Item item) {
        return repository.save(Item.builder()
                .name(item.getName())
                .build());
    }

    public Item create(ItemDTO dto) {
        return repository.save(Item.builder()
                .name(dto.getName())
                .build());
    }

    @Override
    public List<Item> read() {
        return repository.findAll();
    }

    @Override
    public Item read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public Item update(int id, Item Item) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    private ItemDTO getUserOrThrowException(Optional<ItemDTO> ItemDTO) {
        if (ItemDTO.isPresent()) {
            return ItemDTO.get();
        }

        throw new UsernameNotFoundException("Item not found in the database!");
    }
}
