package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.exceptions.NotFoundException;
import kz.iitu.diploma.tours.models.ToursItems;
import kz.iitu.diploma.tours.repositories.ToursItemsRepository;
import kz.iitu.diploma.tours.services.interfaces.ToursItemsService;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ToursItemsServiceImpl implements ToursItemsService {

    private final ToursItemsRepository repository;

    public ToursItemsServiceImpl(ToursItemsRepository toursItemsRepository) {
        this.repository = toursItemsRepository;
    }

    @Override
    public ToursItems create(ToursItems toursItems) {
        return repository.save(ToursItems.builder()
                                    .itemId(toursItems.getItemId())
                                    .tourId(toursItems.getTourId())
                                    .build());
    }

    @Override
    public List<ToursItems> read() {
        return repository.findAll();
    }

    @Override
    public ToursItems read(int id) {
        return repository.findById(id).orElseThrow(() -> {
            return new NotFoundException("ToursItems not found", id);
        });
    }

    @Override
    public ToursItems update(int id, ToursItems toursItems) {
        return null;
    }

    public List<ToursItems> findAllByTourId(int id) {
        return repository.findByTourId(id);
    }

    public List<ToursItems> findAllByItemId(int id) {
        return repository.findByItemId(id);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
