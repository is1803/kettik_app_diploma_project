package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.PlaceDTO;
import kz.iitu.diploma.tours.models.Place;
import kz.iitu.diploma.tours.repositories.PlaceRepository;
import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class PlaceServiceImpl implements PlaceService {

    private final PlaceRepository repository;

    public PlaceServiceImpl(PlaceRepository repository) {
        this.repository = repository;
    }

    @Override
    public Place create(Place place) {
        return repository.save(Place.builder()
                        .name(place.getName())
                        .longitude(place.getLongitude())
                        .latitude(place.getLatitude())
                        .build());
    }

    public Place create(PlaceDTO place) {
        return repository.save(Place.builder()
                .name(place.getName())
                .longitude(place.getLongitude())
                .latitude(place.getLatitude())
                .build());
    }

    @Override
    public List<Place> read() {
        return repository.findAll();
    }

    @Override
    public Place read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public Place update(int id, Place place) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    private PlaceDTO getUserOrThrowException(Optional<PlaceDTO> placeDTO) {
        if (placeDTO.isPresent()) {
            return placeDTO.get();
        }

        throw new UsernameNotFoundException("Place not found in the database!");
    }
}
