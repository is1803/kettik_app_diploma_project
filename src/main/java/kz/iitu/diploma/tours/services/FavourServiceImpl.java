package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.FavourDTO;
import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.repositories.FavourRepository;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class FavourServiceImpl implements FavourService {

    private final FavourRepository repository;

    public FavourServiceImpl(FavourRepository repository) {
        this.repository = repository;
    }

    @Override
    public Favour create(Favour favour) {
        return repository.save(favour.builder()
                .name(favour.getName())
                .build());
    }

    public Favour create(FavourDTO dto) {
        return repository.save(Favour.builder()
                .name(dto.getName())
                .build());
    }

    @Override
    public List<Favour> read() {
        return repository.findAll();
    }

    @Override
    public Favour read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public Favour update(int id, Favour favour) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    private FavourDTO getUserOrThrowException(Optional<FavourDTO> ServiceDTO) {
        if (ServiceDTO.isPresent()) {
            return ServiceDTO.get();
        }

        throw new UsernameNotFoundException("Favour not found in the database!");
    }
}
