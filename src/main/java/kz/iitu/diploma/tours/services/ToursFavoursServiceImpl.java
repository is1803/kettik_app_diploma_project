package kz.iitu.diploma.tours.services;

import kz.iitu.diploma.tours.DTO.tour.ToursFavoursDTO;
import kz.iitu.diploma.tours.models.ToursFavours;
import kz.iitu.diploma.tours.models.ToursItems;
import kz.iitu.diploma.tours.models.ToursPlaces;
import kz.iitu.diploma.tours.repositories.ToursFavoursRepository;
import kz.iitu.diploma.tours.services.interfaces.ToursFavoursService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ToursFavoursServiceImpl implements ToursFavoursService {

    private final ToursFavoursRepository repository;

    public ToursFavoursServiceImpl(ToursFavoursRepository repository) {
        this.repository = repository;
    }

    @Override
    public ToursFavours create(ToursFavours toursFavours) {
        return repository.save(ToursFavours.builder()
                .favourId(toursFavours.getFavourId())
                .tourId(toursFavours.getTourId())
                .build());
    }

    public ToursFavours create(ToursFavoursDTO toursFavours) {
        return repository.save(ToursFavours.builder()
                .tourId(toursFavours.getTourId())
                .favourId(toursFavours.getFavourId())
                .build());
    }

    @Override
    public List<ToursFavours> read() {
        return repository.findAll();
    }

    @Override
    public ToursFavours read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public ToursFavours update(int id, ToursFavours toursFavours) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    public List<ToursFavours> findAllByTourId(int id) {
        return repository.findByTourId(id);
    }

}
