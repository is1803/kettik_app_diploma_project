package kz.iitu.diploma.tours.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tours.models.Favour;

public interface FavourService extends CRUDService<Favour> {
}
