//package kz.iitu.diploma.tours.models;
//
//
//import kz.iitu.diploma.interfaces.AbstractEntity;
//import lombok.*;
//
//import javax.persistence.Entity;
//import javax.persistence.Table;
//
//@Entity
//@Builder
//@Getter
//@Setter
//@AllArgsConstructor
//@Table(name = "tours_dates")
//@NoArgsConstructor(force = true)
//public class ToursDates extends AbstractEntity {
//    private int dateId;
//    private int tourId;
//}
