package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "places")
@NoArgsConstructor(force = true)
public class Place extends AbstractEntity {

    private String name;
    private String longitude;
    private String latitude;

//    @ManyToMany(mappedBy = "places")
//    private List<Tour> tours = new ArrayList<>();
//    @ManyToMany
//    private Set<Tour> tours;
}
