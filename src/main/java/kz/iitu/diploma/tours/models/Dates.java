package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Builder @Getter @Setter
@AllArgsConstructor
@Table(name = "dates")
@NoArgsConstructor(force = true)
public class Dates extends AbstractEntity {

    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private int maxCountOfPerson;

}
