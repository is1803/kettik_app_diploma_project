package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "tours_favours")
@NoArgsConstructor(force = true)
public class ToursFavours extends AbstractEntity {
    private int favourId;
    private int tourId;
}
