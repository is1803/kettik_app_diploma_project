package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.*;

@Entity
@Builder @Getter @Setter
@AllArgsConstructor
@Table(name = "tours")
@NoArgsConstructor(force = true)
public class Tour extends AbstractEntity {

    private String title;
    private String description;
    private int price;

    private int cityId;

    private int ownerId;

    private int tourDateId;
}
