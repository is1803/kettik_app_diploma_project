package kz.iitu.diploma.tours.models;


import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder @Getter @Setter
@AllArgsConstructor
@Table(name = "tours_items")
@NoArgsConstructor(force = true)
public class ToursItems extends AbstractEntity {

    private int itemId;
    private int tourId;
}
