package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "tours_places")
@NoArgsConstructor(force = true)
public class ToursPlaces extends AbstractEntity {

    private int placeId;
    private int tourId;
}