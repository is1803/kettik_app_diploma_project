package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "favours")
@NoArgsConstructor(force = true)
public class Favour extends AbstractEntity {

    private String name;

//    @ManyToMany(mappedBy = "favours")
//    private List<Tour> tours = new ArrayList<>();

//    @ManyToMany
//    private Set<Tour> tours;


}