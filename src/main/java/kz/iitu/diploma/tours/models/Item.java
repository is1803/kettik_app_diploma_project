package kz.iitu.diploma.tours.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Builder @Getter @Setter
@AllArgsConstructor
@Table(name = "items")
@NoArgsConstructor(force = true)
public class Item extends AbstractEntity {

    private String name;

}
