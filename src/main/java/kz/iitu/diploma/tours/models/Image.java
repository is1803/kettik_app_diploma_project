//package kz.iitu.diploma.tours.models;
//
//import kz.iitu.diploma.interfaces.AbstractEntity;
//import lombok.*;
//import javax.persistence.*;
//
//@Entity
//@Builder
//@Getter @Setter
//@AllArgsConstructor
//@Table(name = "images")
//@NoArgsConstructor(force = true)
//public class Image extends AbstractEntity {
//    private String bucket;
//
//    private String url;
//
//    private String filename;
//}
