package kz.iitu.diploma.tours.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "cities")
@NoArgsConstructor(force = true)
public class City extends AbstractEntity {

    private String name;

}
