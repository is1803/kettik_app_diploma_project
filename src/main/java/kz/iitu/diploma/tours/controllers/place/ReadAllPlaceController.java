package kz.iitu.diploma.tours.controllers.place;

import kz.iitu.diploma.tours.models.Place;
import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/places")
public class ReadAllPlaceController {
    private final PlaceService service;

    public ReadAllPlaceController(PlaceService service) {
        this.service = service;
    }

    @GetMapping
    public List<Place> read(){
        return service.read();
    }
}

//[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "KAIYNDY",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "CHARYN",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "LUNA",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        }
//        ]
