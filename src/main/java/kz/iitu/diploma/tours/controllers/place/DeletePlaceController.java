package kz.iitu.diploma.tours.controllers.place;

import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class DeletePlaceController {
    private final PlaceService service;

    public DeletePlaceController(PlaceService service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
