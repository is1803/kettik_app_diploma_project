package kz.iitu.diploma.tours.controllers.tour;

import kz.iitu.diploma.tours.DTO.tour.FinalTourDTO;
import kz.iitu.diploma.tours.services.TourServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tours")
public class ReadTourController {
    private final TourServiceImpl service;

    public ReadTourController(TourServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/get-one/tour/{id}")
    public ResponseEntity<?> read(@PathVariable Integer id){
        FinalTourDTO finalTourDTO = service.findOne(id);
        return ResponseEntity.ok(finalTourDTO);
    }

    @GetMapping("/get-by-businessUserId/{userId}") // 47483647
    public ResponseEntity<?> read(@PathVariable int userId){
        List<FinalTourDTO> finalTourDTOList = service.getByBusinessUserId(userId);
        return ResponseEntity.ok(finalTourDTOList);
    }
}
