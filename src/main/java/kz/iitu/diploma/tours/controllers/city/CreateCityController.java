package kz.iitu.diploma.tours.controllers.city;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cities")
public class CreateCityController {
    private final CityService service;

    public CreateCityController(CityService service) {
        this.service = service;
    }

    @PostMapping("/create-city")
    public City create(@RequestBody City city){
        return service.create(city);
    }
}
