//package kz.iitu.diploma.tours.controllers.tour;
//
//import kz.iitu.diploma.tours.DTO.tour.TourUpdateDTO;
//import kz.iitu.diploma.tours.models.Tour;
//import kz.iitu.diploma.tours.services.TourServiceImpl;
//import kz.iitu.diploma.tours.services.interfaces.PlaceService;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping("/api/places")
//public class UpdateTourController {
//
//    private final TourServiceImpl service;
//
//    public UpdateTourController(TourServiceImpl service) {
//        this.service = service;
//    }
//
//    @PutMapping("/edit/{id}")
//    public Tour update(@PathVariable Integer id,
//                       @RequestBody TourUpdateDTO place){
//        return service.update(id, place);
//    }
//}