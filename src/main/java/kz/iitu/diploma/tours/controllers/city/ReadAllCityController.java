package kz.iitu.diploma.tours.controllers.city;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class ReadAllCityController {
    private final CityService service;

    public ReadAllCityController(CityService service) {
        this.service = service;
    }

    @GetMapping
    public List<City> read(){
        return service.read();
    }
}
