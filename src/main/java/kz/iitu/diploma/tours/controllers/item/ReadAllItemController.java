package kz.iitu.diploma.tours.controllers.item;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.ItemService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/items")
public class ReadAllItemController {
    private final ItemService service;

    public ReadAllItemController(ItemService service) {
        this.service = service;
    }

    @GetMapping
    public List<Item> read(){
        return service.read();
    }
}

//[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "KAIYNDY",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "CHARYN",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "LUNA",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        }
//        ]


//[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "вода"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "плед"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "обед"
//        }
//        ]


//[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "конная прогулка"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "не конная прогулка"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "услуги гида"
//        }
//        ]