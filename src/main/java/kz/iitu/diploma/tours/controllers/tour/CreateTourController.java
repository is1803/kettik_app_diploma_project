package kz.iitu.diploma.tours.controllers.tour;

import kz.iitu.diploma.tours.DTO.tour.TourAddDTO;
import kz.iitu.diploma.tours.services.TourServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tours")
public class CreateTourController {

    private final TourServiceImpl service;

    public CreateTourController(TourServiceImpl service) {
        this.service = service;
    }

    @PostMapping("/create-tour")
    public ResponseEntity<?> create (@RequestBody TourAddDTO tourDTO ){
        service.createTour(tourDTO);
        return ResponseEntity.ok("success");
    }

//    @RequestMapping(value = "/add/image/{tourId}",method = RequestMethod.POST)
//    public void addImage(@RequestParam("files") List<MultipartFile> files,@PathVariable int tourId){
//        service.addImg(files,tourId);
//    }
}
