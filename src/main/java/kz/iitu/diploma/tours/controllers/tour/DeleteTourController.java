package kz.iitu.diploma.tours.controllers.tour;

import kz.iitu.diploma.tours.services.TourServiceImpl;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tours")
public class DeleteTourController {
    private final TourServiceImpl service;

    public DeleteTourController(TourServiceImpl service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
