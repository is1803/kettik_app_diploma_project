package kz.iitu.diploma.tours.controllers.place;

import kz.iitu.diploma.tours.models.Place;
import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class CreatePlaceController {
    private final PlaceService service;

    public CreatePlaceController(PlaceService service) {
        this.service = service;
    }

    @PostMapping("/create-place")
    public Place create(@RequestBody Place place){
        return service.create(place);
    }
}
