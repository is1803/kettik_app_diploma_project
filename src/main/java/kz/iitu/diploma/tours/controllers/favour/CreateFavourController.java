package kz.iitu.diploma.tours.controllers.favour;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/favours")
public class CreateFavourController {
    private final FavourService service;

    public CreateFavourController(FavourService service) {
        this.service = service;
    }

    @PostMapping("/create-favour")
    public Favour create(@RequestBody Favour favour){
        return service.create(favour);
    }
}
