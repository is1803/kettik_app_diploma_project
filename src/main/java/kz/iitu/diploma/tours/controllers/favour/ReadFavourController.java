package kz.iitu.diploma.tours.controllers.favour;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/favours")
public class ReadFavourController {
    private final FavourService service;

    public ReadFavourController(FavourService service) {
        this.service = service;
    }

    @GetMapping("/get-one/favour/{id}")
    public Favour read(@PathVariable Integer id){
        return service.read(id);
    }
}
