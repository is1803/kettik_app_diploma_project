package kz.iitu.diploma.tours.controllers.place;

import kz.iitu.diploma.tours.models.Place;
import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/places")
public class ReadPlaceController {
    private final PlaceService service;

    public ReadPlaceController(PlaceService service) {
        this.service = service;
    }

    @GetMapping("/get-one/place/{id}")
    public Place read(@PathVariable Integer id){
        return service.read(id);
    }
}
