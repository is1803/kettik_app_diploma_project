package kz.iitu.diploma.tours.controllers.city;

import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cities")
public class DeleteCityController {
    private final CityService service;

    public DeleteCityController(CityService service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
