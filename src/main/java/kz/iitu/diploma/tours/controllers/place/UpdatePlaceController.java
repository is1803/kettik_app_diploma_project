package kz.iitu.diploma.tours.controllers.place;

import kz.iitu.diploma.tours.models.Place;
import kz.iitu.diploma.tours.services.interfaces.PlaceService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/places")
public class UpdatePlaceController {

    private final PlaceService service;

    public UpdatePlaceController(PlaceService service) {
        this.service = service;
    }

    @PutMapping("/edit/{id}")
    public Place update(@PathVariable Integer id,
                        @RequestBody Place place){
        return service.update(id, place);
    }
}
