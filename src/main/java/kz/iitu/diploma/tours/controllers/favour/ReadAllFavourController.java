package kz.iitu.diploma.tours.controllers.favour;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/favours")
public class ReadAllFavourController {
    private final FavourService service;

    public ReadAllFavourController(FavourService service) {
        this.service = service;
    }

    @GetMapping
    public List<Favour> read(){
        return service.read();
    }
}

//[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "конная прогулка"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "не конная прогулка"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "наверное еще что то есть там"
//        }
//        ]