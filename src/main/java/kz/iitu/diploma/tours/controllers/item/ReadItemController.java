package kz.iitu.diploma.tours.controllers.item;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.ItemService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/items")
public class ReadItemController {
    private final ItemService service;

    public ReadItemController(ItemService service) {
        this.service = service;
    }

    @GetMapping("/get-one/item/{id}")
    public Item read(@PathVariable Integer id){
        return service.read(id);
    }
}
