package kz.iitu.diploma.tours.controllers.favour;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/favours")
public class UpdateFavourController {

    private final FavourService service;

    public UpdateFavourController(FavourService service) {
        this.service = service;
    }

    @PutMapping("/edit/{id}")
    public Favour update(@PathVariable Integer id,
                         @RequestBody Favour favour){
        return service.update(id, favour);
    }
}
