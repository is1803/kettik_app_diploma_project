package kz.iitu.diploma.tours.controllers.favour;

import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.FavourService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/favours")
public class DeleteFavourController {
    private final FavourService service;

    public DeleteFavourController(FavourService service) {
        this.service = service;
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Integer id){
        service.delete(id);
    }
}
