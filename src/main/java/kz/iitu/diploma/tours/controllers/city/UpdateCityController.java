package kz.iitu.diploma.tours.controllers.city;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/cities")
public class UpdateCityController {

    private final CityService service;

    public UpdateCityController(CityService service) {
        this.service = service;
    }

    @PutMapping("/edit/{id}")
    public City update(@PathVariable Integer id,
                       @RequestBody City city){
        return service.update(id, city);
    }
}
