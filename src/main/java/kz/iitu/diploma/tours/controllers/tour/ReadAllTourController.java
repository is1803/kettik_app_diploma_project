package kz.iitu.diploma.tours.controllers.tour;

import kz.iitu.diploma.tours.DTO.tour.FinalTourDTO;
import kz.iitu.diploma.tours.services.TourServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/tours")
public class ReadAllTourController {
    private final TourServiceImpl service;

    public ReadAllTourController(TourServiceImpl service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<?> read(){
        List<FinalTourDTO> finalTourDTOList = service.all();
        return ResponseEntity.ok(finalTourDTOList);
    }

    @GetMapping("/get-by-cityId/{cityId}")
    private ResponseEntity<?> read(@PathVariable int cityId){
        List<FinalTourDTO> finalTourDTOList = service.getByCity(cityId);
        return ResponseEntity.ok(finalTourDTOList);
    }

//    @GetMapping("/get-by-businessId/{businessId}") - ReadTourController

}
