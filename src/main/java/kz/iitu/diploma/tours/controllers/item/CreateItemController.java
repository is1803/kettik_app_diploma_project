package kz.iitu.diploma.tours.controllers.item;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import kz.iitu.diploma.tours.services.interfaces.ItemService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/items")
public class CreateItemController {
    private final ItemService service;

    public CreateItemController(ItemService service) {
        this.service = service;
    }

    @PostMapping("/create-item")
    public Item create(@RequestBody Item item){
        return service.create(item);
    }
}
