package kz.iitu.diploma.tours.controllers.item;

import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.services.interfaces.ItemService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/items")
public class UpdateItemController {

    private final ItemService service;

    public UpdateItemController(ItemService service) {
        this.service = service;
    }

    @PutMapping("/edit/{id}")
    public Item update(@PathVariable Integer id,
                       @RequestBody Item item){
        return service.update(id, item);
    }
}
