package kz.iitu.diploma.tours.controllers.city;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cities")
public class ReadCityController {
    private final CityService service;

    public ReadCityController(CityService service) {
        this.service = service;
    }

    @GetMapping("/get-one/city/{id}")
    public City read(@PathVariable Integer id){
        return service.read(id);
    }
}
