package kz.iitu.diploma.tours.DTO.tour;

import kz.iitu.diploma.tours.models.*;
import kz.iitu.diploma.users.models.User;
import lombok.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class FinalTourDTO {

    private int tourId;
    private String title;
    private String description;
    private int price;

    private City city;

    private User user;

    private List<Item> items;

    private List<Favour> favours;

    private List<Place> places;

    private Dates tourDates;
}
