package kz.iitu.diploma.tours.DTO.tour;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ToursPlacesDTO {

    private int placeId;
    private int tourId;

}