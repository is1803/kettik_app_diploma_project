package kz.iitu.diploma.tours.DTO.tour;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ToursItemsDTO {

    private int itemId;
    private int tourId;

}
