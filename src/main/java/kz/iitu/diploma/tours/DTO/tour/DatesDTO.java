package kz.iitu.diploma.tours.DTO.tour;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class DatesDTO {

    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private int maxCountOfPerson;

}
