package kz.iitu.diploma.tours.DTO.tour;

import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.models.Item;
import kz.iitu.diploma.tours.models.Place;
import lombok.*;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TourAddDTO {

    private String title;
    private String description;
    private int price;

    private int cityId;

    private int ownerId;

    private List<Item> items;

    private List<Favour> favours;

    private List<Place> places;

    private List<DatesDTO> tourDatesList;

}

//{
//        "title" : "KOLSAY",
//        "description" : "description",
//        "price" : 12000,
//        "cityId" : 2,
//        "ownerId" : 1,
//        "items": [
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "вода"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "плед"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "обед"
//        }
//        ],
//        "favours":[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "конная прогулка"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "не конная прогулка"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "услуги гида"
//        }
//        ],
//        "places":[
//        {
//        "id": 1,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "KAIYNDY",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 2,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "CHARYN",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        },
//        {
//        "id": 3,
//        "created": "2022-04-27T19:10:25",
//        "updated": null,
//        "name": "LUNA",
//        "longitude": "1243564765764",
//        "latitude": "1243564765764"
//        }
//        ],
//        "tourDatesList":[
//        {
//        "fromDate": "2022-04-27T20:17:09",
//        "toDate": "2022-04-27T20:17:09",
//        "maxCountOfPerson": 10
//        },
//        {
//        "fromDate": "2022-04-29T20:17:09",
//        "toDate": "2022-04-29T20:17:09",
//        "maxCountOfPerson": 20
//        },
//        {
//        "fromDate": "2022-05-02T20:17:09",
//        "toDate": "2022-05-02T20:17:09",
//        "maxCountOfPerson": 30
//        }
//        ]
//        }
