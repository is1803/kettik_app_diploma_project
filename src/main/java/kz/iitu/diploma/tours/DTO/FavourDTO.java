package kz.iitu.diploma.tours.DTO;

import lombok.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class FavourDTO{

    @Size(min = 2, max = 70, message = "Имя пользователя должен содержать минимум 2 символа и максимум 30")
    @Pattern(regexp = "^[a-zA-Z]*$", message = "Имя пользователя должен содержать только буквы!")
    private String name;

}