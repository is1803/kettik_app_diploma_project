package kz.iitu.diploma.tours.DTO.tour;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TourDTO {

    private String title;
    private String description;
    private int price;

    private int cityId;

    private int ownerId;

    private int tourDateId;
}
