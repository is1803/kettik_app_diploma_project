package kz.iitu.diploma.tours.DTO;

import lombok.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Getter @Setter @Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class PlaceDTO{

    @Size(min = 2, max = 30, message = "Имя тура должен содержать минимум 2 символа и максимум 30")
    @Pattern(regexp = "^[^\\d\\s]+$", message = "name should contain only letters")
    private String name;

    @Size(min = 2, message = "longitude должен содержать минимум 2 символа и максимум 30")
    private String longitude;

    @Size(min = 2, message = "latitude должен содержать минимум 2 символа и максимум 30")
    private String latitude;

}
