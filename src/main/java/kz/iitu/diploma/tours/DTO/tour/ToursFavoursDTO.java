package kz.iitu.diploma.tours.DTO.tour;


import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ToursFavoursDTO {
    private int favourId;
    private int tourId;
}
