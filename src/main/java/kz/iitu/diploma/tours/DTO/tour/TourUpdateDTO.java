package kz.iitu.diploma.tours.DTO.tour;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TourUpdateDTO {

    private String title;
    private String description;
    private int price;

}
