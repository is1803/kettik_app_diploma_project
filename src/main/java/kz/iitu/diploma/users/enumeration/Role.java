package kz.iitu.diploma.users.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@Getter
@AllArgsConstructor
public enum Role implements GrantedAuthority {

    BUSINESS("Бизнес пользователь"), CLIENT("Клиент пользователь");

    private String russianName;

    @Override
    public String getAuthority() {
        return name();
    }
}
