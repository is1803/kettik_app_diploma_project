package kz.iitu.diploma.users.mappers;

import kz.iitu.diploma.interfaces.AbstractMapper;
import kz.iitu.diploma.users.DTO.UserDTO;
import kz.iitu.diploma.users.models.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper extends AbstractMapper<User, UserDTO> {
    public UserMapper() {
        super(User.class, UserDTO.class);
    }
}
