package kz.iitu.diploma.users.DTO.update;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ClientUserUpdateDTO {

    @NotBlank
    @Size(min = 2, message = "FirstName should contain min 2 symbols")
    private String firstName;

    @NotBlank
    @Size(min = 2, message = "LastName should contain min 2 symbols")
    private String lastName;

    @NotBlank
    @Size(min = 11, max = 11, message = "Phone number should contain min and max 11 symbols")
    private String phoneNumber;

    private String discountCardNumber;
}
