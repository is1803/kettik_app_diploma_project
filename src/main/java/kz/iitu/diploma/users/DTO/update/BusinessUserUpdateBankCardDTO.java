package kz.iitu.diploma.users.DTO.update;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BusinessUserUpdateBankCardDTO {

    @NotBlank
    @Size(min = 3, max = 17, message = "bank name number should contain min 3 and max 11 symbols")
    private String bankName;

    @NotBlank
    @Size(min = 16, max = 16, message = "card number number should contain min and max 16 symbols")
    private String cardNumber;

    @NotBlank
    private String nameOfOrganization;
}
