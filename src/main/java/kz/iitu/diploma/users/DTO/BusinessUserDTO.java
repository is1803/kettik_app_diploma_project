package kz.iitu.diploma.users.DTO;

import kz.iitu.diploma.interfaces.AbstractDTO;
import kz.iitu.diploma.users.models.Avatar;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BusinessUserDTO {

    @NotBlank
    @Size(min = 2, message = "FirstName should contain min 2 symbols")
    private String firstName;

    @NotBlank
    @Size(min = 2, message = "LastName should contain min 2 symbols")
    private String lastName;

    @Email
    @NotBlank
    @Size(min = 3, message = "Email should contain min 3 symbols")
    private String email;

    @NotBlank
    @Size(min = 11, max = 11, message = "Phone number should contain min and max 11 symbols")
    private String phoneNumber;

    private String bankName;

    private String cardNumber;

    private String nameOfOrganization;

//    private Avatar avatar;
}
