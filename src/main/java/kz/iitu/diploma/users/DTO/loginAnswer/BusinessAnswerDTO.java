package kz.iitu.diploma.users.DTO.loginAnswer;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BusinessAnswerDTO {

    private String token;
    private boolean isDetailsFilled;
    private int userId;

}
