package kz.iitu.diploma.users.DTO.add;


import kz.iitu.diploma.interfaces.AbstractDTO;
import kz.iitu.diploma.users.models.Avatar;
import lombok.*;

import javax.validation.constraints.Null;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class BusinessUserDetailsAddDTO extends AbstractDTO {

    private String bankName;
    private String cardNumber;
    private String nameOfOrganization;
    private int userId;

//    @Null
//    private Avatar avatar = null;
}
