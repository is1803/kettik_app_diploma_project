package kz.iitu.diploma.users.DTO.loginAnswer;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ClientAnswerDTO {

    private String token;
    private int userId;

}