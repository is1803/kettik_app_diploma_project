package kz.iitu.diploma.users.DTO.add;

import kz.iitu.diploma.interfaces.AbstractDTO;
import kz.iitu.diploma.users.models.Avatar;
import lombok.*;

import javax.validation.constraints.Null;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class ClientUserDetailsAddDTO extends AbstractDTO {

    private String discountCardNumber;
    private int userId;

}
