package kz.iitu.diploma.users.controllers.user.business;

import kz.iitu.diploma.users.DTO.BusinessUserDTO;
import kz.iitu.diploma.users.services.BusinessUserServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class ReadBusinessUserController {
    private final BusinessUserServiceImpl service;

    public ReadBusinessUserController(BusinessUserServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/get-business/user/{email}")
    public BusinessUserDTO read(@PathVariable String email) {
        return service.getBusinessUser(email);
    }
}
