package kz.iitu.diploma.users.controllers.registration;

import kz.iitu.diploma.exceptions.EntityAlreadyExistsException;
import kz.iitu.diploma.users.DTO.UserDTO;
import kz.iitu.diploma.users.DTO.add.BusinessUserDetailsAddDTO;
import kz.iitu.diploma.users.DTO.add.ClientUserDetailsAddDTO;
import kz.iitu.diploma.users.enumeration.Role;
import kz.iitu.diploma.users.events.RegistrationCompleteEvent;
import kz.iitu.diploma.users.mappers.UserMapper;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.services.BusinessUserDetailsServiceImpl;
import kz.iitu.diploma.users.services.ClientUserDetailsServiceImpl;
import kz.iitu.diploma.users.services.interfaces.UserService;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class RegistrationUserController {

    private final UserService service;
    private final UserMapper mapper;
    private final ClientUserDetailsServiceImpl clientUserService;
    private final BusinessUserDetailsServiceImpl businessUserService;
    private final ApplicationEventPublisher publisher;

    public RegistrationUserController(UserService service,
                                      UserMapper mapper,
                                      ClientUserDetailsServiceImpl clientUserService,
                                      BusinessUserDetailsServiceImpl businessUserService, ApplicationEventPublisher publisher) {
        this.service = service;
        this.mapper = mapper;
        this.clientUserService = clientUserService;
        this.businessUserService = businessUserService;
        this.publisher = publisher;
    }

    @PostMapping("/client-user-registration")
    public ResponseEntity<?> registrationClient(@Valid UserDTO userDTO,
                                          HttpServletRequest request) {
        String email = userDTO.getEmail();

        if (service.existByEmail(email)) {
            throw new EntityAlreadyExistsException("Client user with email - " + email + " already exists!");
        }
//  -------------------------------------------------------------------
        User user = service.create(mapper.toEntity(userDTO));
        service.getByEmail(userDTO.getEmail()).setRole(Role.CLIENT.name());
//  -------------------------------------------------------------------

        publisher.publishEvent(new RegistrationCompleteEvent(user,
                                                                    request.getLocale()));
        ClientUserDetailsAddDTO clientUserDetailsAddDTO = ClientUserDetailsAddDTO.builder()
                                                                .userId(user.getId())
//                                                                .avatar(null)
                                                                .build();
        clientUserService.create(clientUserDetailsAddDTO);
        return ResponseEntity
                            .status(HttpStatus.CREATED)
                            .body(user);
    }

    @PostMapping("/business-user-registration")
    public ResponseEntity<?> registrationBusiness(@Valid UserDTO userDTO,
                                          HttpServletRequest request) {
        String email = userDTO.getEmail();

        if (service.existByEmail(email)) {
            throw new EntityAlreadyExistsException("Business User with email - " + email + " already exists!");
        }
//  -------------------------------------------------------------------
        User user = service.create(mapper.toEntity(userDTO));
        service.getByEmail(userDTO.getEmail()).setRole(Role.BUSINESS.name());
//  -------------------------------------------------------------------

        publisher.publishEvent(new RegistrationCompleteEvent(user,
                request.getLocale()));

        BusinessUserDetailsAddDTO businessUserDetailsAddDTO = BusinessUserDetailsAddDTO.builder()
                .userId(user.getId())
                .build();

        businessUserService.create(businessUserDetailsAddDTO);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(user);
    }

}