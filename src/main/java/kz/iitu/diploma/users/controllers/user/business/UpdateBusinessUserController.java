package kz.iitu.diploma.users.controllers.user.business;

import kz.iitu.diploma.users.DTO.BusinessUserDTO;
import kz.iitu.diploma.users.DTO.update.BusinessUserUpdateBankCardDTO;
import kz.iitu.diploma.users.DTO.update.BusinessUserUpdateDTO;
import kz.iitu.diploma.users.services.BusinessUserServiceImpl;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/users")
public class UpdateBusinessUserController {

    private final BusinessUserServiceImpl service;
//    @Autowired
//    private MinioUtil minioUtil;

    public UpdateBusinessUserController(BusinessUserServiceImpl service) {
        this.service = service;
    }

    @PutMapping("/update-business-user/{id}")
    public BusinessUserDTO update(@PathVariable Integer id,
                                  @RequestBody BusinessUserUpdateDTO businessUser) {
        return service.getUpdatedBusinessUser(id, businessUser);
        //getUpdatedBankBusinessUser
    }

    @PutMapping("/update-business-user-bank/{id}")
    public BusinessUserDTO update(@PathVariable Integer id,
                                  @RequestBody BusinessUserUpdateBankCardDTO businessUser) {
        return service.getUpdatedBankBusinessUser(id, businessUser);
        //getUpdatedBankBusinessUser
    }

//    @RequestMapping(value = "/edit/{id}",method = RequestMethod.POST)
//    public String edit(@PathVariable Integer id,
//                       @RequestPart("file") MultipartFile file,
//                       @Valid UserEditDTO userDTO ){
//        if (!minioUtil.bucketExists("businessUser"))
//            minioUtil.makeBucket("businessUser");
//        minioUtil.putObject("businessUser",file,file.getName(),file.getContentType());
//        service.edit(id, userDTO, file);
//        return "success";
//    }
}
