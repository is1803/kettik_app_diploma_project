package kz.iitu.diploma.users.controllers.user.client;

import kz.iitu.diploma.users.DTO.ClientUserDTO;
import kz.iitu.diploma.users.services.ClientUserServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class ReadClientUserController {
    private final ClientUserServiceImpl service;

    public ReadClientUserController(ClientUserServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/get-client/user/{email}")
    public ClientUserDTO read(@PathVariable String email) {
        return service.getClientUser(email);
    }
}
