package kz.iitu.diploma.users.controllers.user;

import kz.iitu.diploma.users.DTO.UserDTO;
import kz.iitu.diploma.users.DTO.loginAnswer.BusinessAnswerDTO;
import kz.iitu.diploma.users.DTO.loginAnswer.ClientAnswerDTO;
import kz.iitu.diploma.users.enumeration.Role;
import kz.iitu.diploma.users.helpers.JWTTokenGenerator;
import kz.iitu.diploma.users.models.BusinessUserDetails;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.services.BusinessUserDetailsServiceImpl;
import kz.iitu.diploma.users.services.interfaces.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/users")
public class LoginUserController {

    private final UserService service;
    private final BusinessUserDetailsServiceImpl businessUserService;
    private final PasswordEncoder encoder;
    private final JWTTokenGenerator tokenGenerator;

    public LoginUserController(UserService service, BusinessUserDetailsServiceImpl businessUserService, PasswordEncoder encoder, JWTTokenGenerator tokenGenerator) {
        this.service = service;
        this.businessUserService = businessUserService;
        this.encoder = encoder;
        this.tokenGenerator = tokenGenerator;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserDTO userDTO, HttpServletRequest request) {

        User user = service.getByEmail(userDTO.getEmail());
        if (encoder.matches(userDTO.getPassword(), user.getPassword()) && user.getEmail().equals(userDTO.getEmail())) {
            if (user.getRole().equals(Role.BUSINESS.toString())){

                BusinessUserDetails businessUserDetails = businessUserService.read(user.getId());

                if(businessUserDetails.getBankName()==null){
                    BusinessAnswerDTO businessAnswerDTO = new BusinessAnswerDTO();
                    String token = tokenGenerator.generateAccessToken(userDTO.getEmail(), "https://kettik.tour.app");
                    businessAnswerDTO.setToken(token);
                    businessAnswerDTO.setDetailsFilled(false);
                    businessAnswerDTO.setUserId(user.getId());
                    return ResponseEntity.ok(businessAnswerDTO);
                }
                else {
                    BusinessAnswerDTO businessAnswerDTO = new BusinessAnswerDTO();
                    String token = tokenGenerator.generateAccessToken(userDTO.getEmail(), "https://kettik.tour.app");
                    businessAnswerDTO.setToken(token);
                    businessAnswerDTO.setDetailsFilled(true);
                    businessAnswerDTO.setUserId(user.getId());
                    return ResponseEntity.ok(businessAnswerDTO);
                }
            }else {
                ClientAnswerDTO clientAnswerDTO = new ClientAnswerDTO();

                String token = tokenGenerator.generateAccessToken(user.getEmail(), "https://kettik.tour.app");
                clientAnswerDTO.setToken(token);
                clientAnswerDTO.setUserId(user.getId());
                return ResponseEntity.ok(clientAnswerDTO);
            }
        }
        return ResponseEntity.ok("failed");
    }
}
