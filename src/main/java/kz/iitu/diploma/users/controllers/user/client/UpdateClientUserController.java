package kz.iitu.diploma.users.controllers.user.client;

import kz.iitu.diploma.users.DTO.ClientUserDTO;
import kz.iitu.diploma.users.DTO.update.ClientUserUpdateDTO;
import kz.iitu.diploma.users.services.ClientUserServiceImpl;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class UpdateClientUserController {
    private final ClientUserServiceImpl service;
//    @Autowired
//    private MinioUtil minioUtil;

    public UpdateClientUserController(ClientUserServiceImpl service) {
        this.service = service;
    }

    @PutMapping("/update-client-user/{id}")
    public ClientUserDTO update(@PathVariable Integer id,
                                @RequestBody ClientUserUpdateDTO clientUserUpdateDTO) {
        return service.getUpdatedClientUser(id, clientUserUpdateDTO);
    }
}
