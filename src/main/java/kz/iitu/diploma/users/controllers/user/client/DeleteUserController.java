package kz.iitu.diploma.users.controllers.user.client;//package com.example.test_user_login_email_verification_create_authentication.users.controllers.user;
//
//import com.example.test_user_login_email_verification_create_authentication.users.services.interfaces.BusinessUserService;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/api/users")
//public class DeleteUserController {
//    private final BusinessUserService service;
//
//    public DeleteUserController(BusinessUserService service) {
//        this.service = service;
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public void delete(@PathVariable Integer id) {
//        service.delete(id);
//    }
//}
