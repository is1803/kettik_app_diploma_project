package kz.iitu.diploma.users.controllers.registration;

import kz.iitu.diploma.users.exceptions.InvalidValidationTokenException;
import kz.iitu.diploma.users.services.interfaces.VerificationTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/confirm")
public class ConfirmEmailController {
    private final VerificationTokenService service;

    public ConfirmEmailController(VerificationTokenService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<?> confirm(@RequestParam String token) {
        if (!service.existsByToken(token)) {
            throw new InvalidValidationTokenException("Token is not exists!");
        }

        service.confirm(token);

        return ResponseEntity
                        .status(HttpStatus.CREATED)
                        .body("User has been confirmed...");
    }
}
