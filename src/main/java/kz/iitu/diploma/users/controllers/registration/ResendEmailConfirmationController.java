package kz.iitu.diploma.users.controllers.registration;

import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.services.interfaces.MailSenderService;
import kz.iitu.diploma.users.services.interfaces.UserService;
import kz.iitu.diploma.users.services.interfaces.VerificationTokenService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequestMapping("/api/users/confirm")
public class ResendEmailConfirmationController {

    private final UserService userService;
    private final MailSenderService mailSenderService;
    private final VerificationTokenService verificationTokenService;

    public ResendEmailConfirmationController(MailSenderService mailSenderService,
                                             UserService userService,
                                             VerificationTokenService verificationTokenService) {
        this.userService = userService;
        this.mailSenderService = mailSenderService;
        this.verificationTokenService = verificationTokenService;
    }

    @PostMapping("/resend")
    public ResponseEntity<?> resend(@RequestBody Map<String, String> payload) {
        String email = payload.get("email");

        User user = userService.getByEmail(email);
        String token = verificationTokenService.updateTokenByEmail(email);

        mailSenderService.sendEmailVerificationToken(token, user);

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("Confirmation resend");
    }
}
