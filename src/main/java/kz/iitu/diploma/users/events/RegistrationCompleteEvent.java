package kz.iitu.diploma.users.events;

import kz.iitu.diploma.users.models.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

import java.util.Locale;

@Getter
@Setter
public class RegistrationCompleteEvent extends ApplicationEvent {
    private User user;
    private Locale locale;

    public RegistrationCompleteEvent(User user, Locale locale)
    {
        super(user);

        this.user = user;
        this.locale = locale;
    }
}
