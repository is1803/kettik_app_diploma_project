package kz.iitu.diploma.users.configurations;

import kz.iitu.diploma.users.filters.AuthenticationFilter;
import kz.iitu.diploma.users.filters.AuthorizationFilter;
import kz.iitu.diploma.users.helpers.JWTTokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsS;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JWTTokenGenerator tokenGenerator;


    public SecurityConfiguration(BCryptPasswordEncoder passwordEncoder,
                                 JWTTokenGenerator tokenGenerator,
                                 UserDetailsService userDetailsS) {
        this.userDetailsS = userDetailsS;
        this.passwordEncoder = passwordEncoder;
        this.tokenGenerator = tokenGenerator;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder managerBuilder)
            throws Exception {
        managerBuilder
                    .userDetailsService(userDetailsS)
                    .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        AuthenticationFilter authenticationFilter = new AuthenticationFilter(tokenGenerator,
                                                                                authenticationManagerBean());
        authenticationFilter.setFilterProcessesUrl("api/users/login");
//        authenticationFilter.setFilterProcessesUrl("api/users/login");
        httpSecurity
                .csrf()
                    .disable()
                .cors()
                .configurationSource(request -> {
                    CorsConfiguration configuration = new CorsConfiguration();
                    configuration.setAllowedMethods(List.of("GET","POST", "PUT", "DELETE", "OPTIONS"));
                    configuration.setAllowedHeaders(List.of("*"));
                    return configuration;
                });

        httpSecurity
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        httpSecurity.addFilter(authenticationFilter);
        httpSecurity.addFilterAfter(new AuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
