package kz.iitu.diploma.users.services;

import kz.iitu.diploma.users.exceptions.InvalidValidationTokenException;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.models.VerificationToken;
import kz.iitu.diploma.users.repositories.VerificationTokenRepository;
import kz.iitu.diploma.users.services.interfaces.UserService;
import kz.iitu.diploma.users.services.interfaces.VerificationTokenService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class VerificationTokenIml implements VerificationTokenService {

    private final VerificationTokenRepository repository;
    private final UserService userService;

    public VerificationTokenIml(VerificationTokenRepository repository,
                                UserService userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    public boolean existsByToken(String token) {
        return repository.existsByToken(token);
    }

    @Override
    public User getUser(String token) {
        return repository
                .findByToken(token)
                .getUser();
    }

    @Override
    public void create(User user, String token) {
        repository.save(new VerificationToken(user, token));
    }

    @Override
    public String updateTokenByEmail(String email) {
        User user = userService.getByEmail(email);
        VerificationToken verificationToken = repository.findByUser(user);
        String token = UUID.randomUUID().toString();
        verificationToken.updateToken(token);

        repository.save(verificationToken);
        return token;
    }

    @Override
    public void confirm(String token) {
        VerificationToken verificationToken = repository.findByToken(token);
        Date expiryDate = verificationToken.getExpiryDate();

        if (isExpired(expiryDate)) {
            throw new InvalidValidationTokenException("Token is expired!");
        }

        User user = verificationToken.getUser();
        userService.enableById(user.getId());

        repository.delete(verificationToken);
    }

    private boolean isExpired(Date expiryDate) {
        return expiryDate.before(new Date());
    }
}
