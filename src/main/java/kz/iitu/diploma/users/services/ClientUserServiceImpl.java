package kz.iitu.diploma.users.services;

import kz.iitu.diploma.configurations.MinioUtil;
import kz.iitu.diploma.users.DTO.ClientUserDTO;
import kz.iitu.diploma.users.DTO.update.ClientUserUpdateDTO;
import kz.iitu.diploma.users.models.Avatar;
import kz.iitu.diploma.users.models.ClientUserDetails;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.repositories.AvatarRepository;
import kz.iitu.diploma.users.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class ClientUserServiceImpl {

    private final ClientUserDetailsServiceImpl clientUserDetailsService;
    private final UserService userService;
    private final AvatarRepository avatarRepository;

    @Autowired
    private MinioUtil minioUtil;


    public ClientUserServiceImpl(ClientUserDetailsServiceImpl clientUserDetailsService,
                                 UserService userService, AvatarRepository avatarRepository) {
        this.clientUserDetailsService = clientUserDetailsService;
        this.userService = userService;
        this.avatarRepository = avatarRepository;
    }

    public ClientUserDTO getClientUser(String email){

        User user = userService.getByEmail(email);
        System.out.println(user.getEmail() + " " + user.getRole() + " " + user.getId());
        ClientUserDetails clientUserDetails = clientUserDetailsService.read(user.getId());

        return ClientUserDTO.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .discountCardNumber(clientUserDetails.getDiscountCardNumber())
//                .avatar(clientUserDetails.getAvatar())
                .build();
    }

    public ClientUserDTO getUpdatedClientUser(Integer id, ClientUserUpdateDTO userDTO
//                                              MultipartFile file
    ){

//        Avatar avatar = Avatar.builder()
//                .bucket("user")
//                .filename(file.getOriginalFilename())
//                .url(minioUtil.getObjectUrl("user", file.getOriginalFilename()))
//                .build();
//        avatarRepository.save(avatar);

        User user = userService.read(id);

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        userService.save(user);


        ClientUserDetails clientUserDetails = clientUserDetailsService.read(user.getId());
        clientUserDetails.setDiscountCardNumber(userDTO.getDiscountCardNumber());
//        clientUserDetails.setAvatar(avatarRepository.getById(avatar.getId()));
        clientUserDetailsService.update(clientUserDetails);

        return getClientUser(user.getEmail());
    }

}
