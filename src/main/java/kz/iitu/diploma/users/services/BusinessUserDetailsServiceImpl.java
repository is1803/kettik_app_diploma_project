package kz.iitu.diploma.users.services;

import kz.iitu.diploma.users.DTO.add.BusinessUserDetailsAddDTO;
import kz.iitu.diploma.users.models.BusinessUserDetails;
import kz.iitu.diploma.users.models.ClientUserDetails;
import kz.iitu.diploma.users.repositories.BusinessUserDetailsRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BusinessUserDetailsServiceImpl {

    private final BusinessUserDetailsRepository repository;

    public BusinessUserDetailsServiceImpl(BusinessUserDetailsRepository repository) {
        this.repository = repository;
    }

    public void create(BusinessUserDetailsAddDTO dto) {

        repository.save(BusinessUserDetails.builder()
                .bankName(dto.getBankName())
                .cardNumber(dto.getCardNumber())
                .nameOfOrganization(dto.getNameOfOrganization())
                .userId(dto.getUserId())
                .build());
    }

    public List<BusinessUserDetails> read() {
        return repository.findAll();
    }

    public BusinessUserDetails read(int id) {
        BusinessUserDetails optionalUser = repository.getByUserId(id);

        return optionalUser;
    }

    public void update(BusinessUserDetails businessUserDetails){
        repository.save(businessUserDetails);
    }

    public void delete(int id) {
        repository.deleteById(id);
    }

    private BusinessUserDetails getUserOrThrowException(Optional<BusinessUserDetails> user) {
        if (user.isPresent()) {
            return user.get();
        }
        throw new UsernameNotFoundException("Business User Details not found in the database!");
    }
}
