package kz.iitu.diploma.users.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.users.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends CRUDService<User>, UserDetailsService {

    User getByEmail(String email);

    boolean existByEmail(String email);

    void enableById(int id);

    void save(User user);

}
