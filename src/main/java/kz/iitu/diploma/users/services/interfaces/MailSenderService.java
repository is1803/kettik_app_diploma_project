package kz.iitu.diploma.users.services.interfaces;

import kz.iitu.diploma.users.models.User;
import org.springframework.mail.SimpleMailMessage;

public interface MailSenderService {

    void sendEmailVerificationToken(String token, User user);
    SimpleMailMessage constructEmailForUser(String subject, String message, User user);


//  -------- Change Verification Token --------

//    void sendChangePasswordVerificationToken(String token, BusinessUserDetails businessUser);
//    void sendChangePasswordVerificationToken(String token, ClientUserDetails clientUser);
//    void sendChangeEmailVerificationToken(String token, BusinessUserDetails businessUser);
//    void sendChangeEmailVerificationToken(String token, ClientUserDetails clientUser);

//  -------- Change Verification Token end --------

}
