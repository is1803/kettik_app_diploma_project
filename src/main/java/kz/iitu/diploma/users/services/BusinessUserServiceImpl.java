package kz.iitu.diploma.users.services;

import kz.iitu.diploma.configurations.MinioUtil;
import kz.iitu.diploma.users.DTO.BusinessUserDTO;
import kz.iitu.diploma.users.DTO.update.BusinessUserUpdateBankCardDTO;
import kz.iitu.diploma.users.DTO.update.BusinessUserUpdateDTO;
import kz.iitu.diploma.users.models.Avatar;
import kz.iitu.diploma.users.models.BusinessUserDetails;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.repositories.AvatarRepository;
import kz.iitu.diploma.users.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
@Transactional
public class BusinessUserServiceImpl {

    private final BusinessUserDetailsServiceImpl businessUserDetailsService;
    private final UserService userService;
    private final AvatarRepository avatarRepository;

    @Autowired
    private MinioUtil minioUtil;

    public BusinessUserServiceImpl(BusinessUserDetailsServiceImpl businessUserDetailsService,
                                   UserService userService, AvatarRepository avatarRepository) {
        this.businessUserDetailsService = businessUserDetailsService;
        this.userService = userService;
        this.avatarRepository = avatarRepository;
    }

    public BusinessUserDTO getBusinessUser(String email){
        User user = userService.getByEmail(email);
        BusinessUserDetails businessUserDetails = businessUserDetailsService.read(user.getId());

        return BusinessUserDTO.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .bankName(businessUserDetails.getBankName())
                .cardNumber(businessUserDetails.getCardNumber())
                .nameOfOrganization(businessUserDetails.getNameOfOrganization())
//                .avatar(businessUserDetails.getAvatar())
                .build();
    }

    public BusinessUserDTO getUpdatedBusinessUser(Integer id, BusinessUserUpdateDTO userDTO
//                                                  MultipartFile file
    ){

//        Avatar avatar = Avatar.builder()
//                .bucket("user")
//                .filename(file.getOriginalFilename())
//                .url(minioUtil.getObjectUrl("user", file.getOriginalFilename()))
//                .build();
//        avatarRepository.save(avatar);

        User user = userService.read(id);
        BusinessUserDetails businessUserDetails = businessUserDetailsService.read(user.getId());

        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setPhoneNumber(userDTO.getPhoneNumber());

        userService.save(user);

        businessUserDetails.setBankName(userDTO.getBankName());
        businessUserDetails.setCardNumber(userDTO.getCardNumber());
        businessUserDetails.setNameOfOrganization(userDTO.getNameOfOrganization());
//        businessUserDetails.setAvatar(avatarRepository.getById(avatar.getId()));

        businessUserDetailsService.update(businessUserDetails);

        return getBusinessUser(user.getEmail());
    }

    public BusinessUserDTO getUpdatedBankBusinessUser(Integer id, BusinessUserUpdateBankCardDTO userDTO
    ){
        User user = userService.read(id);
        BusinessUserDetails businessUserDetails = businessUserDetailsService.read(user.getId());
        businessUserDetails.setBankName(userDTO.getBankName());
        businessUserDetails.setCardNumber(userDTO.getCardNumber());
        businessUserDetails.setNameOfOrganization(userDTO.getNameOfOrganization());
//        businessUserDetails.setAvatar(avatarRepository.getById(avatar.getId()));

        businessUserDetailsService.update(businessUserDetails);

        return getBusinessUser(user.getEmail());
    }

}
