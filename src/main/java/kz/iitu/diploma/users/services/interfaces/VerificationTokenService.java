package kz.iitu.diploma.users.services.interfaces;

import kz.iitu.diploma.users.models.User;

public interface VerificationTokenService {

    User getUser(String token);
    void create(User user, String token);

    String updateTokenByEmail(String email);
    boolean existsByToken(String token);
    void confirm(String token);
}
