package kz.iitu.diploma.users.services;

import kz.iitu.diploma.users.DTO.add.ClientUserDetailsAddDTO;
import kz.iitu.diploma.users.models.BusinessUserDetails;
import kz.iitu.diploma.users.models.ClientUserDetails;
import kz.iitu.diploma.users.repositories.ClientUserDetailsRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ClientUserDetailsServiceImpl {

    private final ClientUserDetailsRepository repository;

    public ClientUserDetailsServiceImpl(ClientUserDetailsRepository repository) {
        this.repository = repository;
    }

    public void create(ClientUserDetailsAddDTO dto) {

      repository.save(ClientUserDetails.builder()
                .discountCardNumber(dto.getDiscountCardNumber())
                .userId(dto.getUserId())
//                .avatar(null)
                .build());
    }

    public List<ClientUserDetails> read() {
        return repository.findAll();
    }

    public ClientUserDetails read(int id) {
        ClientUserDetails optionalUser = repository.getByUserId(id);

        return optionalUser;
    }

    public void update(ClientUserDetails clientUserDetails){
        repository.save(clientUserDetails);
    }

    public void delete(int id) {
        repository.deleteById(id);
    }

    private ClientUserDetails getUserOrThrowException(Optional<ClientUserDetails> user) {
        if (user.isPresent()) {
            return user.get();
        }
        throw new UsernameNotFoundException("Client User Details not found in the database!");
    }
}
