package kz.iitu.diploma.users.services;

import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.repositories.UserRepository;
import kz.iitu.diploma.users.services.interfaces.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PasswordEncoder encoder;

    public UserServiceImpl(UserRepository repository,
                                 PasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    @Override
    public User create(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return repository.save(user);
    }

    @Override
    public List<User> read() {
        return repository.findAll();
    }

    @Override
    public User read(int id) {
        Optional<User> optionalUser = repository.findById(id);

        return getUserOrThrowException(optionalUser);
    }

    @Override
    public User update(int id, User user) {
        return user;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Optional<User> optionalUser = repository.findByEmail(email);
        User user = getUserOrThrowException(optionalUser);

        String password = user.getPassword();
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();

        return new org.springframework.security.core.userdetails.User(email,
                password, authorities);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getByEmail(email);
    }

    @Override
    public boolean existByEmail(String email) {
        return repository.existsByEmail(email);
    }

    @Override
    public void enableById(int id) {
        User user = repository.getById(id);
        user.setActive(true);
        save(user);
    }

    public void save(User user){
        repository.save(user);
    }

    private User getUserOrThrowException(Optional<User> user) {
        if (user.isPresent()) {
            return user.get();
        }

        throw new UsernameNotFoundException("User not found in the database!");
    }
}
