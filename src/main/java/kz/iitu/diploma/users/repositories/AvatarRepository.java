package kz.iitu.diploma.users.repositories;

import kz.iitu.diploma.users.models.Avatar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AvatarRepository extends JpaRepository<Avatar, Integer>, JpaSpecificationExecutor<Avatar> {
}
