package kz.iitu.diploma.users.repositories;

import kz.iitu.diploma.users.models.ClientUserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientUserDetailsRepository extends JpaRepository<ClientUserDetails,
        Integer> {
    ClientUserDetails getByUserId(int id);
//    Optional<ClientUserDetails> findByEmail(String email);
//    boolean existsByEmail(String email);
}
