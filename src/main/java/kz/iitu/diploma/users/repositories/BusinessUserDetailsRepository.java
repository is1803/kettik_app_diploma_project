package kz.iitu.diploma.users.repositories;

import kz.iitu.diploma.users.models.BusinessUserDetails;
import kz.iitu.diploma.users.models.ClientUserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BusinessUserDetailsRepository extends JpaRepository<BusinessUserDetails,
        Integer> {
    BusinessUserDetails getByUserId(int id);
//    Optional<BusinessUserDetails> findByEmail(String email);
//    boolean existsByEmail(String email);
}

