package kz.iitu.diploma.users.repositories;

import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.models.VerificationToken;
//import kz.iitu.diploma.users.models.VerificationTokenClientUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Integer> {
    boolean existsByToken(String token);

    VerificationToken findByToken(String token);
    VerificationToken findByUser(User user);
}
