package kz.iitu.diploma.users.repositories;

import kz.iitu.diploma.users.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User getByEmail(String email);
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
}
