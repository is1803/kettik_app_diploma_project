package kz.iitu.diploma.users.models;


import kz.iitu.diploma.interfaces.AbstractEntity;
import kz.iitu.diploma.users.enumeration.Role;
import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "users")
@NoArgsConstructor(force = true)
public class User extends AbstractEntity {

    private String firstName;
    private String lastName;

    private String email;
    private String phoneNumber;
    private String password;

    private boolean isActive;

    private String role;
}
