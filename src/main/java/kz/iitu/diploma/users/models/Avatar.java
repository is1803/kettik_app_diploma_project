package kz.iitu.diploma.users.models;

import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "avatars")
@NoArgsConstructor(force = true)
public class Avatar extends AbstractEntity {

    private String bucket;

    private String url;

    private String filename;
}
