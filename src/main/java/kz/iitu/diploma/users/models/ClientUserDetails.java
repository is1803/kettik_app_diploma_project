package kz.iitu.diploma.users.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "client_user_details")
@NoArgsConstructor(force = true)
public class ClientUserDetails extends AbstractEntity {

    private String discountCardNumber;
    private int userId;

//    @OneToOne(targetEntity = Avatar.class, fetch = FetchType.EAGER)
//    @JoinTable(name = "client_user_details_avatars")
//    private Avatar avatar;

//    @ManyToMany(targetEntity = Avatar.class, fetch = FetchType.EAGER)
//    private List<Tour> tourList = new ArrayList<>();

}
