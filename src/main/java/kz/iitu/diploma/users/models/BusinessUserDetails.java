package kz.iitu.diploma.users.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.*;

@Entity
@Builder
@Getter @Setter
@AllArgsConstructor
@Table(name = "business_user_details")
@NoArgsConstructor(force = true)
public class BusinessUserDetails extends AbstractEntity {

    private String bankName;
    private String cardNumber;
    private String nameOfOrganization;
    private int userId;

//    @OneToOne(targetEntity = Avatar.class, fetch = FetchType.EAGER)
//    @JsonIgnore
//    private Avatar avatar = null;

}
