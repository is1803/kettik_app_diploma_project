package kz.iitu.diploma.tickets.services;

import kz.iitu.diploma.tickets.DTO.client.OrderDTO;
import kz.iitu.diploma.tickets.models.Order;
import kz.iitu.diploma.tickets.repositories.OrderRepository;
import kz.iitu.diploma.tickets.services.interfaces.OrderService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    public OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    public Order createOrder(OrderDTO order) {
        return repository.save(Order.builder()
                .tourId(order.getTourId())
                .ticketId(order.getTicketId())
                .businessId(order.getBusinessId())
                .build()
        );
    }

    public Order findByTicketId(int id) {
        return repository.findByTicketId(id);
    }

    public Order findByTourId(int id) {
        return repository.findByTourId(id);
    }

    public List<Order> findByBusinessUserId(int id) {
        return repository.findAllByBusinessId(id);
    }

    @Override
    public Order create(Order order) {
        return null;
    }

    @Override
    public List<Order> read() {
        return repository.findAll();
    }

    @Override
    public Order read(int id) {
        return repository.findById(id).get();
    }

    @Override
    public Order update(int id, Order order) {
        return null;
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
