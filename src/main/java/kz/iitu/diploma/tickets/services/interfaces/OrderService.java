package kz.iitu.diploma.tickets.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tickets.models.Order;

public interface OrderService extends CRUDService<Order> {
}
