package kz.iitu.diploma.tickets.services.interfaces;

import kz.iitu.diploma.interfaces.CRUDService;
import kz.iitu.diploma.tickets.models.Ticket;

public interface TicketService extends CRUDService<Ticket> {
}
