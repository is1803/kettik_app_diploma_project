package kz.iitu.diploma.tickets.services;

import kz.iitu.diploma.tickets.DTO.business.AnswerClientDTO;
import kz.iitu.diploma.tickets.DTO.business.RequestForBusinessUserDTO;
import kz.iitu.diploma.tickets.DTO.client.*;
import kz.iitu.diploma.tickets.models.Order;
import kz.iitu.diploma.tickets.models.Status;
import kz.iitu.diploma.tickets.models.Ticket;
import kz.iitu.diploma.tickets.repositories.TicketRepository;
import kz.iitu.diploma.tickets.services.interfaces.TicketService;
import kz.iitu.diploma.tours.DTO.tour.FinalTourDTO;
import kz.iitu.diploma.tours.models.Dates;
import kz.iitu.diploma.tours.models.Tour;
import kz.iitu.diploma.tours.services.CityServiceImpl;
import kz.iitu.diploma.tours.services.DatesServiceImpl;
import kz.iitu.diploma.tours.services.TourServiceImpl;
import kz.iitu.diploma.users.models.User;
import kz.iitu.diploma.users.services.UserServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final OrderServiceImpl orderService;
    private final TourServiceImpl tourService;
    private final DatesServiceImpl datesService;
    private final UserServiceImpl userService;
    private final CityServiceImpl cityService;

    public TicketServiceImpl(TicketRepository ticketRepository, OrderServiceImpl orderService, TourServiceImpl tourService, DatesServiceImpl datesService, UserServiceImpl userService, CityServiceImpl cityService) {
        this.ticketRepository = ticketRepository;
        this.orderService = orderService;
        this.tourService = tourService;
        this.datesService = datesService;
        this.userService = userService;
        this.cityService = cityService;
    }

    public Ticket createTicket(TicketDTO ticket) {
        Ticket ticket1 = ticketRepository.save(Ticket.builder()
                        .amountOfPerson(ticket.getAmountOfPerson())
                        .clientId(ticket.getClientId())
                        .tourId(ticket.getTourId())
                        .priceOfOneTicket(ticket.getPriceOfOneTicket())
                        .status(ticket.getStatus())
                        .orderNumber(ticket.getOrderNumber())
                        .totalPrice(ticket.getTotalPrice())
                .build());

        FinalTourDTO tour = tourService.findOne(ticket1.getTourId());

        OrderDTO orderDTO = OrderDTO.builder()
                .ticketId(ticket1.getId())
                .businessId(tour.getUser().getId())
                .tourId(ticket.getTourId())
                .build();

        orderService.createOrder(orderDTO);

        return ticket1;
    }

    public Order getOrderByTicketId(int id) {
        return orderService.findByTicketId(id);
    }

    public List<OrderForClientDTO> getOrderHistoryByClientId(int clientId){

        List<Ticket> tickets = ticketRepository.findByClientId(clientId);
        List<Order> orders = new ArrayList<>();
        List<OrderForClientDTO> orderForClientDTOS = new ArrayList<>();

        for (int i = 0; i < tickets.size(); i++) {
            Ticket ticket = ticketRepository.getById(tickets.get(i).getId());
            orders.add(getOrderByTicketId(ticket.getId()));
        }

        for (int i = 0; i < orders.size(); i++) {

            Ticket ticket = read(orders.get(i).getTicketId());
            Order order = orderService.read(orders.get(i).getId());
            Tour tour = tourService.getOne(order.getTourId());
            Dates dates = datesService.read(tour.getTourDateId());

            OrderForClientDTO orderForClientDTO = OrderForClientDTO.builder()
                    .orderNumber(ticket.getOrderNumber())
                    .tourTitle(tour.getTitle())
                    .createdTime(ticket.getCreated())
                    .tourDate(dates.getFromDate())
                    .build();

            orderForClientDTOS.add(orderForClientDTO);
        }

        return orderForClientDTOS;
    }

    public List<AnswerClientDTO> getClientsOfTour(int id){
        Tour tour = tourService.getOne(id);
        List<Ticket> tickets = getTicketsByTourId(tour.getId());
        List<AnswerClientDTO> answerClientDTOS = new ArrayList<>();

        for (int i = 0; i < tickets.size(); i++) {
            Ticket ticket = read(tickets.get(i).getId());
            User user = userService.read(ticket.getClientId());
            AnswerClientDTO answerClientDTO = AnswerClientDTO.builder()
                    .name(user.getFirstName())
                    .surname(user.getLastName())
                    .phoneNumber(user.getPhoneNumber())
                    .statusOfTicket(ticket.getStatus())
                    .build();
            answerClientDTOS.add(answerClientDTO);
        }

        return answerClientDTOS;
    }

    public List<Ticket> getTicketsByTourId(int id){
        return ticketRepository.findByTourId(id);
    }

    public List<RequestForBusinessUserDTO> findAllOrdersByBusinessUserId(int id){

        List<Order> orders = orderService.findByBusinessUserId(id);
        List<Ticket> tickets = new ArrayList<>();
        List<RequestForBusinessUserDTO> requestForBusinessUserDTOS = new ArrayList<>();

        for (int i = 0; i < orders.size(); i++) {
            tickets.add( read(orders.get(i).getTicketId()));
        }

        for (int i = 0; i < tickets.size(); i++) {
            Tour tour = tourService.getOne(tickets.get(i).getTourId());
            Dates dates = datesService.read(tour.getTourDateId());
            User user = userService.read(tickets.get(i).getClientId());

            RequestForBusinessUserDTO requestForBusinessUserDTO = RequestForBusinessUserDTO.builder()
                    .status(tickets.get(i).getStatus())
                    .clientName(user.getFirstName())
                    .tourDate(dates.getFromDate())
                    .ticketId(tickets.get(i).getId())
                    .tourName(tour.getTitle())
                    .build();

            requestForBusinessUserDTOS.add(requestForBusinessUserDTO);
        }

        return requestForBusinessUserDTOS;
    }

    public List<MyTourDTO> getAllToursOfClient(int clientId){
        List<MyTourDTO> myTourDTOS = new ArrayList<>();
        List<Ticket> tickets = ticketRepository.findByClientId(clientId);
        List<Order> orders = new ArrayList<>();

        for (int i = 0; i < tickets.size(); i++) {
            orders.add(orderService.findByTicketId(tickets.get(i).getId()));
        }

        for (int i = 0; i < orders.size(); i++) {
            MyTourDTO myTourDTO = MyTourDTO.builder()
                    .ticketStatus(read(orders.get(i).getTicketId()).getStatus())
                    .cityName(cityService.read(tourService.findOne(orders.get(i).getTourId()).getCity().getId()).getName())
                    .tourTitle(tourService.findOne(orders.get(i).getTourId()).getTitle())
                    .tourId(orders.get(i).getTourId())
                    .build();
            myTourDTOS.add(myTourDTO);
        }
        return myTourDTOS;
    }

    // my tour details
    public MyTourDetailDTO getTourDetailsOfClient(int clientId, int tourId){

        List<Ticket> tickets = ticketRepository.findByClientId(clientId);
        Order order = null;

        for (Ticket ticket : tickets) {
            if (ticket.getTourId() == tourId) {
                order = orderService.findByTourId(tourId);
            }
        }

        assert order != null;

        return MyTourDetailDTO.builder()
                    .ticketStatus(read(order.getTicketId()).getStatus())
                    .tourName(tourService.findOne(order.getTourId()).getTitle())
                    .fromDate(tourService.findOne(order.getTourId()).getTourDates().getFromDate())
                    .items(tourService.findOne(order.getTourId()).getItems())
                    .favours(tourService.findOne(order.getTourId()).getFavours())
                    .build();
    }

    public RequestForBusinessUserDTO updateTicket(int ticketId, String newStatus){

        Ticket ticket = read(ticketId);
        if(newStatus.equals("Оплачено")){
            ticket.setStatus(Status.PAID.getName());
        }else if(newStatus.equals("Отклонено")){
            ticket.setStatus(Status.REJECTED.getName());
        }else {
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("Something is wrong");
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
            System.out.println("-".repeat(20));
        }

        Tour tour = tourService.getOne(ticket.getTourId());
        Dates dates = datesService.read(tour.getTourDateId());
        User user = userService.read(ticket.getClientId());

        RequestForBusinessUserDTO requestForBusinessUserDTO = RequestForBusinessUserDTO.builder()
                .status(ticket.getStatus())
                .clientName(user.getFirstName())
                .tourDate(dates.getFromDate())
                .ticketId(ticket.getId())
                .tourName(tour.getTitle())
                .build();

        return requestForBusinessUserDTO;
    }

    @Override
    public Ticket create(Ticket ticket) {
        return null;
    }

    @Override
    public List<Ticket> read() {
        return ticketRepository.findAll();
    }

    @Override
    public Ticket read(int id) {
        return ticketRepository.findById(id).get();
    }

    @Override
    public Ticket update(int id, Ticket ticket) {
        return null;
    }

    @Override
    public void delete(int id) {
        ticketRepository.deleteById(id);
    }
}
