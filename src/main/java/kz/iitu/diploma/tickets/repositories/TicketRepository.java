package kz.iitu.diploma.tickets.repositories;

import kz.iitu.diploma.tickets.models.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository  extends JpaRepository<Ticket,Integer>, JpaSpecificationExecutor<Ticket> {

    List<Ticket> findByClientId(int id);
    List<Ticket> findByTourId(int id);
}
