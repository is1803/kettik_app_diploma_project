package kz.iitu.diploma.tickets.repositories;


import kz.iitu.diploma.tickets.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Integer>, JpaSpecificationExecutor<Order> {

    List<Order> findAllByBusinessId(int id);
    List<Order> findAllByTicketId(int id);
    List<Order> findAllByTourId(int id);
    Order findByTicketId(int id);
    Order findByTourId(int id);
//    Order findByBusinessId(int id);

}
