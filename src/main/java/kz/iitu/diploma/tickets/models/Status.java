package kz.iitu.diploma.tickets.models;


import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Status {

    IN_PROGRESS("В процессе"), PAID("Оплачено"), REJECTED("Отклонено");

    private final String name;

}
