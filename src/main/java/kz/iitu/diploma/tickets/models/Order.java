package kz.iitu.diploma.tickets.models;


import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "orders")
@NoArgsConstructor(force = true)
public class Order extends AbstractEntity {

    private int tourId;
    private int ticketId;
    private int businessId;
}
