package kz.iitu.diploma.tickets.models;


import kz.iitu.diploma.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@Table(name = "tickets")
@NoArgsConstructor(force = true)
public class Ticket extends AbstractEntity {

    private int amountOfPerson;
    private int totalPrice;
    private int tourId;
    private String status = Status.IN_PROGRESS.getName();
    private int clientId;
    private int priceOfOneTicket;
    private String orderNumber = UUID.randomUUID().toString();

}


