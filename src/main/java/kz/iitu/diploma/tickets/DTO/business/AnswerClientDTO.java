package kz.iitu.diploma.tickets.DTO.business;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class AnswerClientDTO {

    private String name;
    private String surname;
    private String phoneNumber;
    private String statusOfTicket;

}
