package kz.iitu.diploma.tickets.DTO.business;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class RequestForBusinessUserDTO {

    private String status;
    private String clientName;
    private LocalDateTime tourDate;
    private int ticketId;
    private String tourName;

}
