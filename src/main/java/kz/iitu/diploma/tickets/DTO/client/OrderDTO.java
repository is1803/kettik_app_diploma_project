package kz.iitu.diploma.tickets.DTO.client;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class OrderDTO {

    private int tourId;
    private int ticketId;
    private int businessId;
}
