package kz.iitu.diploma.tickets.DTO.client;

import kz.iitu.diploma.tours.models.Favour;
import kz.iitu.diploma.tours.models.Item;
import lombok.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class MyTourDetailDTO {

    private String tourName; // +
    private LocalDateTime fromDate;
    private String ticketStatus; // +
    private List<Item> items;
    private List<Favour> favours;

}
