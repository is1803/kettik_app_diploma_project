package kz.iitu.diploma.tickets.DTO.client;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class MyTourDTO {

    private String tourTitle;
    private String ticketStatus;
    private String cityName;
    private int tourId;
//    private String img;
}
