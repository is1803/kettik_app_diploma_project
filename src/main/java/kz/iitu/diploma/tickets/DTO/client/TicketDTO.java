package kz.iitu.diploma.tickets.DTO.client;

import kz.iitu.diploma.tickets.models.Status;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class TicketDTO {
    private int amountOfPerson;
    private int totalPrice;
    private int tourId;
    private int clientId;
    private int priceOfOneTicket;
    private String status = Status.IN_PROGRESS.getName();
    private String orderNumber = UUID.randomUUID().toString();
}
