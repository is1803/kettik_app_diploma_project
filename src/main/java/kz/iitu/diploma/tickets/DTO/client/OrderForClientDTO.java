package kz.iitu.diploma.tickets.DTO.client;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class OrderForClientDTO {

    private String orderNumber;
    private String tourTitle;
    private LocalDateTime createdTime;
    private LocalDateTime tourDate;
}
