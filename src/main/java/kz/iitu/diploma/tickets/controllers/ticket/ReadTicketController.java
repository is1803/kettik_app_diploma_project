package kz.iitu.diploma.tickets.controllers.ticket;

import kz.iitu.diploma.tickets.DTO.client.MyTourDetailDTO;
import kz.iitu.diploma.tickets.models.Ticket;
import kz.iitu.diploma.tickets.services.TicketServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tickets")
public class ReadTicketController {
    private final TicketServiceImpl service;

    public ReadTicketController(TicketServiceImpl service) {
        this.service = service;
    }

    @GetMapping("/get-one-ticket-by-ticketId/{ticketId}")
    public Ticket read(@PathVariable int ticketId){
        return service.read(ticketId);
    }

    @GetMapping("/getTourDetailsOfClient/{clientId}/{tourId}")
    public MyTourDetailDTO getTourDetailsOfClient(@PathVariable int clientId, @PathVariable int tourId){
        return service.getTourDetailsOfClient(clientId, tourId);
    }


}
