//package kz.iitu.diploma.tickets.controllers.ticket;
//
//import kz.iitu.diploma.tours.services.interfaces.CityService;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@RestController
//@RequestMapping("/api/cities")
//public class DeleteTicketController {
//    private final CityService service;
//
//    public DeleteTicketController(CityService service) {
//        this.service = service;
//    }
//
//    @DeleteMapping("/delete/{id}")
//    public void delete(@PathVariable Integer id){
//        service.delete(id);
//    }
//}
