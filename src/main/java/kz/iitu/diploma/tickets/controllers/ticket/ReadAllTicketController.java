package kz.iitu.diploma.tickets.controllers.ticket;

import kz.iitu.diploma.tickets.DTO.business.AnswerClientDTO;
import kz.iitu.diploma.tickets.DTO.client.MyTourDTO;
import kz.iitu.diploma.tickets.models.Ticket;
import kz.iitu.diploma.tickets.services.TicketServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tickets")
public class ReadAllTicketController {
    private final TicketServiceImpl service;

    public ReadAllTicketController(TicketServiceImpl service) {
        this.service = service;
    }

    @GetMapping
    public List<Ticket> read(){
        return service.read();
    }

    @GetMapping("/getClientsOfTour/{tourId}")
    public List<AnswerClientDTO> getClientsOfTour(@PathVariable int tourId){
        return service.getClientsOfTour(tourId);
    }

//    getAllToursOfClient

    @GetMapping("/getAllToursOfClient/{clientId}")
    public List<MyTourDTO> getAllToursOfClient(@PathVariable int clientId){
        return service.getAllToursOfClient(clientId);
    }
}
