package kz.iitu.diploma.tickets.controllers.ticket;

import kz.iitu.diploma.tickets.DTO.client.TicketDTO;
import kz.iitu.diploma.tickets.models.Ticket;
import kz.iitu.diploma.tickets.services.TicketServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/tickets")
public class CreateTicketController {
    private final TicketServiceImpl service;

    public CreateTicketController(TicketServiceImpl service) {
        this.service = service;
    }

    @PostMapping("/create-ticket")
    public Ticket create(@RequestBody TicketDTO ticketDTO){
        return service.createTicket(ticketDTO);
    }
}
