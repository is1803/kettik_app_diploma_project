package kz.iitu.diploma.tickets.controllers.ticket;

import kz.iitu.diploma.tickets.DTO.business.RequestForBusinessUserDTO;
import kz.iitu.diploma.tickets.services.TicketServiceImpl;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tickets")
public class UpdateTicketController {

    private final TicketServiceImpl service;

    public UpdateTicketController(TicketServiceImpl service) {
        this.service = service;
    }

    @PutMapping("/edit/{ticketId}")
    public RequestForBusinessUserDTO update(@PathVariable Integer ticketId,
                                            @RequestParam String newStatus){
        return service.updateTicket(ticketId, newStatus);
    }
}
