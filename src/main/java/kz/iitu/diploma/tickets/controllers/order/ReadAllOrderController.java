package kz.iitu.diploma.tickets.controllers.order;

import kz.iitu.diploma.tickets.DTO.client.OrderForClientDTO;
import kz.iitu.diploma.tickets.DTO.business.RequestForBusinessUserDTO;
import kz.iitu.diploma.tickets.models.Order;
import kz.iitu.diploma.tickets.services.TicketServiceImpl;
import kz.iitu.diploma.tickets.services.interfaces.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class ReadAllOrderController {
    private final TicketServiceImpl service;
    private final OrderService orderService;

    public ReadAllOrderController(TicketServiceImpl service, OrderService orderService) {
        this.service = service;
        this.orderService = orderService;
    }

    @GetMapping
    public List<Order> read(){
        return orderService.read();
    }

    @GetMapping("/get-order-history-client/{clientID}")
    public List<OrderForClientDTO> read(@PathVariable int clientID){
        return service.getOrderHistoryByClientId(clientID);
    }

    @GetMapping("/get-order-history-business/{businessID}")
    public List<RequestForBusinessUserDTO> findAllOrdersByBusinessUserId(@PathVariable int businessID){
        return service.findAllOrdersByBusinessUserId(businessID);
    }
}
