package kz.iitu.diploma.tickets.controllers.order;

import kz.iitu.diploma.tours.models.City;
import kz.iitu.diploma.tours.services.interfaces.CityService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/orders")
public class ReadOrderController {
    private final CityService service;

    public ReadOrderController(CityService service) {
        this.service = service;
    }

    @GetMapping("/get-one/order/{id}")
    public City read(@PathVariable Integer id){
        return service.read(id);
    }
}
