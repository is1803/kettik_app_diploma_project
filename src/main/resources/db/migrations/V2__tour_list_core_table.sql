--Модуль Favour, ItemService, Cities, Places.

-- Module CITY

CREATE TABLE cities
(
    "id"        SERIAL       NOT NULL,
    "created"   TIMESTAMP    NOT NULL,
    "updated"   TIMESTAMP    DEFAULT NULL,
    "name"      VARCHAR(50)  DEFAULT NULL,

    CONSTRAINT cities_pkey
        PRIMARY KEY(id)
);

INSERT INTO cities(id, created, updated, name)
VALUES
       (1, '2022-04-27 19:10:25-07', null, 'Almaty'),
       (2, '2022-04-27 19:10:25-07', null, 'Nur-Sultan'),
       (3, '2022-04-27 19:10:25-07', null, 'Shymkent'),
       (4, '2022-04-27 19:10:25-07', null, 'Karaganda'),
       (5, '2022-04-27 19:10:25-07', null, 'Oskemen'),
       (6, '2022-04-27 19:10:25-07', null, 'Atyrau'),
       (7, '2022-04-27 19:10:25-07', null, 'Aktau'),
       (8, '2022-04-27 19:10:25-07', null, 'Ust-Kamenagorsk'),
       (9, '2022-04-27 19:10:25-07', null, 'Uralsk');

-- -----------------------------------------------------------------------


-- Module ITEM FAVOUR PLACE

CREATE TABLE items
(
    "id"        SERIAL       NOT NULL,
    "created"   TIMESTAMP    NOT NULL,
    "updated"   TIMESTAMP    DEFAULT NULL,
    "name"      VARCHAR(50)  DEFAULT NULL,

    CONSTRAINT items_pkey
        PRIMARY KEY(id)
);

INSERT INTO items(id, created, updated, name)
VALUES
    (47483647, '2022-04-27 19:10:25-07', null, 'water'),
    (47483648, '2022-04-27 19:10:25-07', null, 'blanket'),
    (47483649, '2022-04-27 19:10:25-07', null, 'lunch');

-- -----------------------------------------------------------------------


CREATE TABLE favours
(
    "id"        SERIAL       NOT NULL,
    "created"   TIMESTAMP    NOT NULL,
    "updated"   TIMESTAMP    DEFAULT NULL,
    "name"      VARCHAR(50)  DEFAULT NULL,

    CONSTRAINT favours_pkey
        PRIMARY KEY(id)
);

INSERT INTO favours(id, created, updated, name)
VALUES
    (47483647, '2022-04-27 19:10:25-07', null, 'horse riding'),
    (47483648, '2022-04-27 19:10:25-07', null, 'camping'),
    (47483649, '2022-04-27 19:10:25-07', null, 'guide services');

-- -----------------------------------------------------------------------


CREATE TABLE places
(
    "id"         SERIAL       NOT NULL,
    "created"    TIMESTAMP    NOT NULL,
    "updated"    TIMESTAMP    DEFAULT NULL,
    "name"       VARCHAR(50)  DEFAULT NULL,
    "longitude"  VARCHAR(50)  DEFAULT NULL,
    "latitude"   VARCHAR(50)  DEFAULT NULL,

    CONSTRAINT places_pkey
        PRIMARY KEY(id)
);

INSERT INTO places(id, created, updated, name, longitude, latitude)
VALUES
    (47483647, '2022-04-27 19:10:25-07', null, 'Kolsai Lake', '78.4622968851', '42.9815650746'), -- 1
    (47483648, '2022-04-27 19:10:25-07', null, 'Charyn Canyon - Valley of Castles', '79.385254817','43.7747370579'), -- 2
    (47483649, '2022-04-27 19:10:25-07', null, 'Black Canyon', '-107.716666667', '38.5666666667'), -- 3
    (47483650, '2022-04-27 19:10:25-07', null, 'Moon Canyon', '78.357037', '45.011254'), -- 4
    (47483651, '2022-04-27 19:10:25-07', null, 'Bartogai Reservoir', '76.9568115341', '43.2379354935'), -- 5
    (47483652, '2022-04-27 19:10:25-07', null, 'Kayyndy Lake', '73.683052', ' 42.829755'), -- 6
    (47483653, '2022-04-27 19:10:25-07', null, 'Tanbaly Tas', '1243564765764', '1243564765764'), -- 7
    (47483654, '2022-04-27 19:10:25-07', null, 'The Assy Plateau', '77.00876', '43.2808'), -- 8
    (47483655, '2022-04-27 19:10:25-07', null, 'Turgen Waterfall', '77.6572146512', '43.297191371'), -- 9
    (47483656, '2022-04-27 19:10:25-07', null, 'Lake Tuzkol', '79.385254817', '43.7747370579'), -- 10
    (47483657, '2022-04-27 19:10:25-07', null, 'Tekes Waterfall', '73.42702264006006', '55.04494571799219'), -- 11
    (47483658, '2022-04-27 19:10:25-07', null, 'Issyk lake', '73.42702264006006', '55.04494571799219'), -- 12
    (47483659, '2022-04-27 19:10:25-07', null, 'Trout farming', '73.42702264006006', '55.04494571799219'), -- 13
    (47483660, '2022-04-27 19:10:25-07', null, 'Bear Waterfall', '73.42702264006006', '55.04494571799219'); -- 14

-- -----------------------------------------------------------------------



-- Module Dates

CREATE TABLE dates
(
    "id"                    SERIAL       NOT NULL,
    "created"               TIMESTAMP    NOT NULL,
    "updated"               TIMESTAMP    DEFAULT NULL,
    "from_date"             TIMESTAMP    DEFAULT NULL,
    "to_date"               TIMESTAMP    DEFAULT NULL,
    "max_count_of_person"   SERIAL       NOT NULL,

    CONSTRAINT tour_dates_pkey
        PRIMARY KEY(id)
);

INSERT INTO dates(id, created, updated, from_date, to_date, max_count_of_person)
VALUES
    (47483641, '2022-04-27 19:10:25-07', null, '2022-07-01 19:10:25-07', '2022-07-01 19:10:25-07', '20'),
    (47483642, '2022-04-27 19:10:26-07', null, '2022-07-03 19:10:25-07', '2022-07-05 19:10:25-07', '20'),
    (47483643, '2022-04-27 19:10:27-07', null, '2022-07-05 19:10:25-07', '2022-07-05 19:10:25-07', '20'),
    (47483644, '2022-04-27 19:10:28-07', null, '2022-07-06 19:10:25-07', '2022-07-08 19:10:25-07', '20'),
    (47483645, '2022-04-27 19:10:29-07', null, '2022-07-07 19:10:25-07', '2022-07-07 19:10:25-07', '20'),
    (47483646, '2022-04-27 19:10:30-07', null, '2022-07-08 19:10:25-07', '2022-07-10 19:10:25-07', '20'),
    (47483647, '2022-04-27 19:10:31-07', null, '2022-07-09 19:10:25-07', '2022-07-09 19:10:25-07', '20'),
    (47483648, '2022-04-27 19:10:32-07', null, '2022-07-10 19:10:25-07', '2022-07-12 19:10:25-07', '20'),
    (47483649, '2022-04-27 19:10:33-07', null, '2022-07-11 19:10:25-07', '2022-07-11 19:10:25-07', '20'),
    (47483650, '2022-04-27 19:10:34-07', null, '2022-07-12 19:10:25-07', '2022-07-12 19:10:25-07', '20');

-- ------------------------------------------------------------------------


-- Module TOUR

CREATE TABLE tours
(
    "id"              SERIAL       NOT NULL,
    "created"         TIMESTAMP    NOT NULL,
    "updated"         TIMESTAMP    DEFAULT NULL,
    "title"           VARCHAR(500)  DEFAULT NULL,
    "description"     VARCHAR(5000)  DEFAULT NULL,
    "price"           SERIAL       NOT NULL,
    "city_id"         SERIAL       NOT NULL,
    "owner_id"        SERIAL       NOT NULL,
    "tour_date_id"    SERIAL       NOT NULL,

    CONSTRAINT tours_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_tours_cities
        FOREIGN KEY (city_id)
            REFERENCES cities (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tours_users
        FOREIGN KEY (owner_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tours_tour_dates
        FOREIGN KEY (tour_date_id)
            REFERENCES dates (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO tours(id, created, updated, title, description, price, city_id, owner_id, tour_date_id)
VALUES
    (47483647, '2022-04-27 19:10:25-07', null, 'Issyk Lake, Turgen waterfall, Trout farming',
                                                       'The most beautiful and magical corner near Almaty. An ideal ' ||
                                                       'vacation for those who do not want to travel for a long time, ' ||
                                                       'while at the same time taking a break from the hustle and bustle ' ||
                                                       'of the city. Who wants to have an atmospheric and soulful time.. ' ||
                                                       'Your attention is drawn to a wonderful budget tour to Issyk Lake. ' ||
                                                       'The trip is only for 6000 tenge, we will ' ||
                                                       'visit 2 locations, have a good rest and just have an unreal day.',
                                                        6000, 1, 47483647, 47483641),
    (47483648, '2022-04-27 19:10:25-07', null, 'Kolsai Lake, Kayyndy Lake, Black Canyon',
                                                       'Mountains, lakes, fir trees and clean air. Boats, horses and ' ||
                                                       'delicious food  ️. Games, dancing, karaoke and atmospheric trip .' ||
                                                       ' We are sure you have heard, seen and even want to visit! But, ' ||
                                                       'not enough time? Then catch our express tour! In just 1 day we' ||
                                                       ' will visit these places with a professional guide, convenient ' ||
                                                       'transport and the best atmosphere ',
                                                        11000, 1, 47483647, 47483642),
    (47483649, '2022-04-27 19:10:25-07', null, 'Kolsai Lake, Charyn canyon "Valley of Castles", Black Canyon, Moon Canyon, Bartogai reservoir',
                                                       'We have so much beauty, just 3-5 hours away. And there is an ' ||
                                                       'opportunity to see all this, breathe that air and enjoy nature ' ||
                                                       'for just -10000 tenge. In just one day we will visit an incredible ' ||
                                                       '5 locations. This is the rest you need. Away from the city, the ' ||
                                                       'hustle and bustle, in the clean air, with cool guys and incredible ' ||
                                                       'views.',
                                                        10000, 1, 47483647, 47483643),
    (47483650, '2022-04-27 19:10:25-07', null, 'Assi Plateau, Bear Waterfall',
                                                       'Starry sky, tents, hot pilaf, guitar songs, romance, gorgeous nature,' ||
                                                       ' majestic mountains around, a cool guide, and new acquaintances - its ' ||
                                                       'all about the Assi Plateau with an overnight stay! Just imagine this ' ||
                                                       'atmosphere. We are going to the most beautiful zhailau of southern ' ||
                                                       'Kazakhstan, to watch the most beautiful sunset  to make a reboot ' ||
                                                       'from routine and top content. No need to look for tents, cook food,' ||
                                                       ' carry heavy loads… We have provided everything for you  you just ' ||
                                                       'need to read the info and sign up',
                                                        16500, 1, 47483647, 47483644),
    (47483651, '2022-04-27 19:10:25-07', null, 'Kolsai Lake', 'The most beautiful and magical corner near Almaty. An ideal ' ||
                                                       'vacation for those who do not want to travel for a long time, ' ||
                                                       'while at the same time taking a break from the hustle and bustle ' ||
                                                       'of the city. Who wants to have an atmospheric and soulful time.. ' ||
                                                       'Your attention is drawn to a wonderful budget tour to Kolsai Lake. ' ||
                                                       'The trip is only for 6000 tenge, we will ' ||
                                                       'visit 2 locations, have a good rest and just have an unreal day.',
                                                        6000, 1, 47483647, 47483645),
    (47483652, '2022-04-27 19:10:25-07', null, 'Kolsai Lake', 'The most beautiful and magical corner near Almaty. An ideal ' ||
                                                       'vacation for those who do not want to travel for a long time, ' ||
                                                       'while at the same time taking a break from the hustle and bustle ' ||
                                                       'of the city. Who wants to have an atmospheric and soulful time.. ' ||
                                                       'Your attention is drawn to a wonderful budget tour to Kolsai Lake. ' ||
                                                       'The trip is only for 6000 tenge, we will ' ||
                                                       'visit 2 locations, have a good rest and just have an unreal day.',
                                                        6000, 1, 47483647, 47483646),
    (47483653, '2022-04-27 19:10:25-07', null, 'Kolsai Lake', 'The most beautiful and magical corner near Almaty. An ideal ' ||
                                                       'vacation for those who do not want to travel for a long time, ' ||
                                                       'while at the same time taking a break from the hustle and bustle ' ||
                                                       'of the city. Who wants to have an atmospheric and soulful time.. ' ||
                                                       'Your attention is drawn to a wonderful budget tour to Kolsai Lake. ' ||
                                                       'The trip is only for 6000 tenge, we will ' ||
                                                       'visit 2 locations, have a good rest and just have an unreal day.',
                                                        6000, 1, 47483647, 47483647)
       ;

-- ------------------------------------------------------------------------


-- Module TOUR + ITEM

CREATE TABLE tours_items
(
    "id"              SERIAL       NOT NULL,
    "created"         TIMESTAMP    NOT NULL,
    "updated"         TIMESTAMP    DEFAULT NULL,
    "tour_id"         SERIAL       NOT NULL,
    "item_id"         SERIAL       NOT NULL,

    CONSTRAINT tours_items_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_tours_items_tours
        FOREIGN KEY (tour_id)
            REFERENCES tours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tours_items_items
        FOREIGN KEY (item_id)
            REFERENCES items (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO tours_items(id, created, updated, tour_id, item_id)
VALUES
    (47483640, '2022-04-27 19:10:25-07', null, 47483647, 47483647),
    (47483641, '2022-04-27 19:10:25-07', null, 47483647, 47483648),
    (47483642, '2022-04-27 19:10:25-07', null, 47483647, 47483649),
    (47483643, '2022-04-27 19:10:25-07', null, 47483648, 47483647),
    (47483644, '2022-04-27 19:10:25-07', null, 47483648, 47483648),
    (47483645, '2022-04-27 19:10:25-07', null, 47483648, 47483649),
    (47483646, '2022-04-27 19:10:25-07', null, 47483649, 47483647),
    (47483647, '2022-04-27 19:10:25-07', null, 47483649, 47483648),
    (47483648, '2022-04-27 19:10:25-07', null, 47483649, 47483649),
    (47483649, '2022-04-27 19:10:25-07', null, 47483650, 47483647),
    (47483650, '2022-04-27 19:10:25-07', null, 47483650, 47483648),
    (47483651, '2022-04-27 19:10:25-07', null, 47483650, 47483649),
    (47483652, '2022-04-27 19:10:25-07', null, 47483651, 47483647),
    (47483653, '2022-04-27 19:10:25-07', null, 47483651, 47483648),
    (47483654, '2022-04-27 19:10:25-07', null, 47483651, 47483649),
    (47483655, '2022-04-27 19:10:25-07', null, 47483652, 47483647),
    (47483656, '2022-04-27 19:10:25-07', null, 47483652, 47483648),
    (47483657, '2022-04-27 19:10:25-07', null, 47483652, 47483649),
    (47483658, '2022-04-27 19:10:25-07', null, 47483653, 47483647),
    (47483659, '2022-04-27 19:10:25-07', null, 47483653, 47483648),
    (47483660, '2022-04-27 19:10:25-07', null, 47483653, 47483649);

-- ------------------------------------------------------------------------


-- Module TOUR + FAVOURS

CREATE TABLE tours_favours
(
    "id"              SERIAL       NOT NULL,
    "created"         TIMESTAMP    NOT NULL,
    "updated"         TIMESTAMP    DEFAULT NULL,
    "tour_id"         SERIAL       NOT NULL,
    "favour_id"         SERIAL       NOT NULL,

    CONSTRAINT tours_favours_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_tours_favours_tours
        FOREIGN KEY (tour_id)
            REFERENCES tours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tours_favours_items
        FOREIGN KEY (favour_id)
            REFERENCES favours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO tours_favours(id, created, updated, tour_id, favour_id)
VALUES
    (47483640, '2022-04-27 19:10:25-07', null, 47483647, 47483647),
    (47483641, '2022-04-27 19:10:25-07', null, 47483647, 47483648),
    (47483642, '2022-04-27 19:10:25-07', null, 47483647, 47483649),
    (47483643, '2022-04-27 19:10:25-07', null, 47483648, 47483647),
    (47483644, '2022-04-27 19:10:25-07', null, 47483648, 47483648),
    (47483645, '2022-04-27 19:10:25-07', null, 47483648, 47483649),
    (47483646, '2022-04-27 19:10:25-07', null, 47483649, 47483647),
    (47483647, '2022-04-27 19:10:25-07', null, 47483649, 47483648),
    (47483648, '2022-04-27 19:10:25-07', null, 47483649, 47483649),
    (47483649, '2022-04-27 19:10:25-07', null, 47483650, 47483647),
    (47483650, '2022-04-27 19:10:25-07', null, 47483650, 47483648),
    (47483651, '2022-04-27 19:10:25-07', null, 47483650, 47483649),
    (47483652, '2022-04-27 19:10:25-07', null, 47483651, 47483647),
    (47483653, '2022-04-27 19:10:25-07', null, 47483651, 47483648),
    (47483654, '2022-04-27 19:10:25-07', null, 47483651, 47483649),
    (47483655, '2022-04-27 19:10:25-07', null, 47483652, 47483647),
    (47483656, '2022-04-27 19:10:25-07', null, 47483652, 47483648),
    (47483657, '2022-04-27 19:10:25-07', null, 47483652, 47483649),
    (47483658, '2022-04-27 19:10:25-07', null, 47483653, 47483647),
    (47483659, '2022-04-27 19:10:25-07', null, 47483653, 47483648),
    (47483660, '2022-04-27 19:10:25-07', null, 47483653, 47483649);

-- ------------------------------------------------------------------------


-- Module TOUR + PLACES

CREATE TABLE tours_places
(
    "id"              SERIAL       NOT NULL,
    "created"         TIMESTAMP    NOT NULL,
    "updated"         TIMESTAMP    DEFAULT NULL,
    "tour_id"         SERIAL       NOT NULL,
    "place_id"         SERIAL       NOT NULL,

    CONSTRAINT tours_places_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_tours_places_tours
        FOREIGN KEY (tour_id)
            REFERENCES tours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tours_places_items
        FOREIGN KEY (place_id)
            REFERENCES places (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


INSERT INTO tours_places(id, created, updated, tour_id, place_id)
VALUES
    (47483641, '2022-04-27 19:10:25-07', null, 47483647, 47483655),
    (47483642, '2022-04-27 19:10:25-07', null, 47483647, 47483658),
    (47483643, '2022-04-27 19:10:25-07', null, 47483647, 47483659),
    (47483644, '2022-04-27 19:10:25-07', null, 47483648, 47483647),
    (47483645, '2022-04-27 19:10:25-07', null, 47483648, 47483652),
    (47483646, '2022-04-27 19:10:25-07', null, 47483648, 47483649),
    (47483647, '2022-04-27 19:10:25-07', null, 47483649, 47483647),
    (47483648, '2022-04-27 19:10:25-07', null, 47483649, 47483648),
    (47483649, '2022-04-27 19:10:25-07', null, 47483649, 47483649),
    (47483650, '2022-04-27 19:10:25-07', null, 47483649, 47483650),
    (47483651, '2022-04-27 19:10:25-07', null, 47483649, 47483651),
    (47483652, '2022-04-27 19:10:25-07', null, 47483650, 47483654),
    (47483653, '2022-04-27 19:10:25-07', null, 47483650, 47483660),
    (47483654, '2022-04-27 19:10:25-07', null, 47483651, 47483647),
    (47483655, '2022-04-27 19:10:25-07', null, 47483652, 47483647),
    (47483656, '2022-04-27 19:10:25-07', null, 47483653, 47483647);

-- ------------------------------------------------------------------------