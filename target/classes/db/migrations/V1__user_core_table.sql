--Модуль атворизации и аутентификации, Users и Roles.

CREATE TABLE users
(
    "id"           SERIAL          NOT NULL,
    "created"      TIMESTAMP    NOT NULL,
    "updated"      TIMESTAMP    DEFAULT NULL,
    "first_name"   VARCHAR(50)  NOT NULL,
    "last_name"    VARCHAR(50)  NOT NULL,
    "phone_number" VARCHAR(12)  NOT NULL,
    "email"        VARCHAR(50)  NOT NULL,
    "password"     VARCHAR      NOT NULL,
    "is_active"    BOOLEAN      DEFAULT FALSE,
    "role"         VARCHAR(50),

    CONSTRAINT users_pkey
        PRIMARY KEY(id),

    CONSTRAINT users_phone_number_unique
        UNIQUE (phone_number),

    CONSTRAINT users_email_unique
        UNIQUE (email)
);

-- Business TestUser's password: testPassword, email: test.testov.testovich.84@gmail.com
-- Client TestUser's password: clientsTestPassword, email: arlanvladelets@gmail.com

INSERT INTO users(id, created, updated, first_name, last_name, phone_number, email, password, is_active, role)
VALUES
    (47483647, '2022-05-21 17:10:48.760393', '2022-05-21 17:10:57.638166', 'Business', 'TestUser', '87001112233', 'test.testov.testovich.84@gmail.com', '$2a$10$evgD5g43XFq7cjIJNisRpO9iePHmT3kzTzqgSwGvOm/tkRax28NTO', true, 'BUSINESS'),
    (47483648, '2022-05-21 17:30:55.262832', '2022-05-21 17:30:55.310706', 'Client', 'TestUser', '87071112233', 'arlanvladelets@gmail.com', '$2a$10$..zpOwlraewOlIFQdBy/q.tc4iKbWaXUeiA0MvaOEUC9sQKTXZ1Tu', true, 'CLIENT')
    ;

CREATE TABLE business_user_details
(
    "id"           SERIAL       NOT NULL,
    "created"      TIMESTAMP    NOT NULL,
    "updated"      TIMESTAMP    DEFAULT NULL,
    "bank_name"    VARCHAR(50)  DEFAULT NULL,
    "card_number"  VARCHAR      DEFAULT NULL,
    "user_id"      INT          NOT NULL,
    "name_of_organization"    VARCHAR(50)  DEFAULT NULL,

    CONSTRAINT business_users_details_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_users_business_user_details
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO business_user_details(id, created, updated, bank_name, card_number, user_id, name_of_organization)
VALUES
    (47483647, '2022-05-21 17:10:48.760393', '2022-05-21 17:10:57.638166', 'Halyk bank', '1234123412341234', 47483647, 'Kettik organization');

CREATE TABLE client_user_details
(
    "id"                    SERIAL       NOT NULL,
    "created"               TIMESTAMP    NOT NULL,
    "updated"               TIMESTAMP    DEFAULT NULL,
    "discount_card_number"  VARCHAR,
    "user_id"               INT          NOT NULL,

    CONSTRAINT client_users_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_users_client_user_details
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

INSERT INTO client_user_details(id, created, updated, discount_card_number, user_id)
VALUES
    (47483648, '2022-05-21 17:30:59.429027', null, '1234123412341234', 47483648);

CREATE TABLE verification_tokens
(
    "id"           SERIAL,
    "created"      TIMESTAMP NOT NULL,
    "updated"      TIMESTAMP DEFAULT NULL,
    "user_id"      INT    NOT NULL,
    "token"        VARCHAR   NOT NULL,
    "expiry_date"  TIMESTAMP NOT NULL,

    CONSTRAINT verification_tokens_users_pkey
        PRIMARY KEY(id),

    CONSTRAINT business_user_token_unique
        UNIQUE (token),

    CONSTRAINT fk_users_verification_tokens
        FOREIGN KEY (user_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);


--Модуль аватар картинки.

CREATE TABLE avatars
(
    "id"          SERIAL     NOT NULL,
    "created"     TIMESTAMP  NOT NULL,
    "updated"     TIMESTAMP  DEFAULT NULL,
    "bucket"      varchar    NOT NULL,
    "url"         varchar    NOT NULL,
    "filename"    varchar    NOT NULL,

    CONSTRAINT avatars_pkey
        PRIMARY KEY(id)
);