-- Module TicketRepository

CREATE TABLE tickets
(
    "id"                  SERIAL       NOT NULL,
    "created"             TIMESTAMP    NOT NULL,
    "updated"             TIMESTAMP    DEFAULT NULL,
    "amount_of_person"    SERIAL       NOT NULL,
    "total_price"         SERIAL       NOT NULL,
    "tour_id"             SERIAL       NOT NULL,
    "status"              VARCHAR       NOT NULL,
    "client_id"           SERIAL       NOT NULL,
    "price_of_one_ticket" SERIAL       NOT NULL,
    "order_number"        VARCHAR       NOT NULL,

    CONSTRAINT tickets_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_tickets_tours
        FOREIGN KEY (tour_id)
            REFERENCES tours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_tickets_users
        FOREIGN KEY (client_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);

CREATE TABLE orders
(
    "id"                  SERIAL       NOT NULL,
    "created"             TIMESTAMP    NOT NULL,
    "updated"             TIMESTAMP    DEFAULT NULL,
    "tour_id"             SERIAL       NOT NULL,
    "ticket_id"           SERIAL       NOT NULL,
    "business_id"           SERIAL       NOT NULL,

    CONSTRAINT orders_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_orders_tours
        FOREIGN KEY (tour_id)
            REFERENCES tours (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_orders_tickets
        FOREIGN KEY (ticket_id)
            REFERENCES tickets (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE,

    CONSTRAINT fk_orders_users
        FOREIGN KEY (business_id)
            REFERENCES users (id)
            ON DELETE RESTRICT
            ON UPDATE CASCADE
);